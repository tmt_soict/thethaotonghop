#import "SWGCheckPriceParamBody.h"

@implementation SWGCheckPriceParamBody

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.vehicleQuality = @1;
    self.vehicleType = @4;
    self.operatorId = @0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"distance": @"distance", @"load": @"load", @"time": @"time", @"from": @"from", @"to": @"to", @"is_sharing": @"isSharing", @"vehicle_quality": @"vehicleQuality", @"vehicle_type": @"vehicleType", @"operator_id": @"operatorId" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"distance", @"load", @"time", @"from", @"to", @"isSharing", @"vehicleQuality", @"vehicleType", @"operatorId"];
  return [optionalProperties containsObject:propertyName];
}

@end
