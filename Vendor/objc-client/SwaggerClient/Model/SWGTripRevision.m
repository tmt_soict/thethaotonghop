#import "SWGTripRevision.h"

@implementation SWGTripRevision

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"editor": @"editor", @"time": @"time", @"trip_info": @"tripInfo", @"trip_detail": @"tripDetail" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"tripInfo", @"tripDetail"];
  return [optionalProperties containsObject:propertyName];
}

@end
