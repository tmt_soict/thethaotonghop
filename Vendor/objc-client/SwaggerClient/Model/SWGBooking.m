#import "SWGBooking.h"

@implementation SWGBooking

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"booking_code": @"bookingCode", @"booking_id": @"bookingId", @"request_id": @"requestId", @"ref_trip_status": @"refTripStatus", @"passenger_id": @"passengerId", @"from": @"from", @"to": @"to", @"pickup_time": @"pickupTime", @"estimate_pickup_time": @"estimatePickupTime", @"dropoff_time": @"dropoffTime", @"load": @"load", @"is_sharing": @"isSharing", @"status": @"status", @"distance": @"distance", @"price_range": @"priceRange", @"vehicle_quality": @"vehicleQuality", @"vehicle_type": @"vehicleType", @"operator_id": @"operatorId" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"bookingCode", @"bookingId", @"requestId", @"passengerId", @"vehicleQuality", @"vehicleType", @"operatorId"];
  return [optionalProperties containsObject:propertyName];
}

@end
