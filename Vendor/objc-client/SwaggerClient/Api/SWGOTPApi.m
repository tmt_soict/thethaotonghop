#import "SWGOTPApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGGetOTPResponseValue.h"
#import "SWGVerifyAccountParamBody.h"
#import "SWGVerifyAccountResponseValue.h"


@interface SWGOTPApi ()

@property (nonatomic, strong) NSMutableDictionary *defaultHeaders;

@end

@implementation SWGOTPApi

NSString* kSWGOTPApiErrorDomain = @"SWGOTPApiErrorDomain";
NSInteger kSWGOTPApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    self = [super init];
    if (self) {
        SWGConfiguration *config = [SWGConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[SWGApiClient alloc] init];
        }
        _apiClient = config.apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+ (instancetype)sharedAPI {
    static SWGOTPApi *sharedAPI;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.defaultHeaders[key];
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self setDefaultHeaderValue:value forKey:key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(NSUInteger) requestQueueSize {
    return [SWGApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Get otp code by phone.
/// Get otp code by phone
///  @param phone  
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGGetOTPResponseValue*
///
-(NSNumber*) getOTPWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGGetOTPResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'phone' is set
    if (phone == nil) {
        NSParameterAssert(phone);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"phone"] };
            NSError* error = [NSError errorWithDomain:kSWGOTPApiErrorDomain code:kSWGOTPApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/otp/{phone}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (phone != nil) {
        pathParams[@"phone"] = phone;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetOTPResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetOTPResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get otp code by phone auth.
/// Get otp code by phone auth
///  @param phone  
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGGetOTPResponseValue*
///
-(NSNumber*) getOTPAuthWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGGetOTPResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'phone' is set
    if (phone == nil) {
        NSParameterAssert(phone);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"phone"] };
            NSError* error = [NSError errorWithDomain:kSWGOTPApiErrorDomain code:kSWGOTPApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/otp/authen/{phone}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (phone != nil) {
        pathParams[@"phone"] = phone;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetOTPResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetOTPResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Verify account by otp code
/// Verify account by otp code
///  @param phone  
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGVerifyAccountResponseValue*
///
-(NSNumber*) verifyAccountWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGVerifyAccountParamBody*) body
    completionHandler: (void (^)(SWGVerifyAccountResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'phone' is set
    if (phone == nil) {
        NSParameterAssert(phone);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"phone"] };
            NSError* error = [NSError errorWithDomain:kSWGOTPApiErrorDomain code:kSWGOTPApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/otp/{phone}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (phone != nil) {
        pathParams[@"phone"] = phone;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGVerifyAccountResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGVerifyAccountResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Verify account by otp code auth
/// Verify account by otp code auth
///  @param phone  
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGVerifyAccountResponseValue*
///
-(NSNumber*) verifyAccountAuthWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGVerifyAccountParamBody*) body
    completionHandler: (void (^)(SWGVerifyAccountResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'phone' is set
    if (phone == nil) {
        NSParameterAssert(phone);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"phone"] };
            NSError* error = [NSError errorWithDomain:kSWGOTPApiErrorDomain code:kSWGOTPApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/otp/authen/{phone}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (phone != nil) {
        pathParams[@"phone"] = phone;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGVerifyAccountResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGVerifyAccountResponseValue*)data, error);
                                }
                           }
          ];
}



@end
