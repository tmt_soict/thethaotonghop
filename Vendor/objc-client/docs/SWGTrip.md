# SWGTrip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**driverId** | **NSNumber*** |  | 
**startTime** | **NSNumber*** |  | 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**totalPassengers** | **NSNumber*** |  | 
**status** | **NSNumber*** | trip status. 0&#x3D;waiting, 10&#x3D;running, 15&#x3D;finished, 16&#x3D;canceled | 
**income** | **NSNumber*** | total price of trip | 
**route** | [**NSArray&lt;SWGLocationPoint&gt;***](SWGLocationPoint.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


