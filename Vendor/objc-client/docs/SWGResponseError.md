# SWGResponseError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **NSNumber*** | error code | 
**message** | **NSString*** | error message | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


