# SWGGetBookingMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastId** | **NSNumber*** | id of last item / last date | 
**minSize** | **NSNumber*** | minimum items / request | 
**orderType** | **NSNumber*** | 0 &#x3D; order by created time new -&gt; old | 1 &#x3D; order bycreated time old -&gt; new | 2 &#x3D; order by start time new -&gt; old | 3 &#x3D; order by start time old -&gt; new | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


