# SWGGetAllBookingsOfDateResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**selectedDate** | **NSNumber*** |  | 
**bookings** | [**NSArray&lt;SWGBooking&gt;***](SWGBooking.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


