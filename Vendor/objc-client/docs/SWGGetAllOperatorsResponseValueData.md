# SWGGetAllOperatorsResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**operators** | [**NSArray&lt;SWGOperator&gt;***](SWGOperator.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


