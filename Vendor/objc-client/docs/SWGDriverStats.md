# SWGDriverStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalIncome** | **NSNumber*** |  | 
**totalTrips** | **NSNumber*** |  | 
**completedTrips** | **NSNumber*** |  | 
**totalRating** | **NSNumber*** |  | 
**averageRating** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


