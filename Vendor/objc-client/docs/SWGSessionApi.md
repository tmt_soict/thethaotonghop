# SWGSessionApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**callRegister**](SWGSessionApi.md#callregister) | **POST** /session/register | Register new account
[**changePassword**](SWGSessionApi.md#changepassword) | **PUT** /session/changepass | Change password of authenticated user
[**login**](SWGSessionApi.md#login) | **POST** /session/login | Login with phone &amp; password
[**resetPassword**](SWGSessionApi.md#resetpassword) | **POST** /session/resetpass | Reset password
[**sessionAuthLogin**](SWGSessionApi.md#sessionauthlogin) | **POST** /session/authen/login | Login with phone &amp; password
[**sessionAuthRegister**](SWGSessionApi.md#sessionauthregister) | **POST** /session/authen/register | Register new account
[**sessionAuthResetPwd**](SWGSessionApi.md#sessionauthresetpwd) | **POST** /session/authen/resetpass | Reset password


# **callRegister**
```objc
-(NSNumber*) callRegisterWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGRegisterParamBody*) body
        completionHandler: (void (^)(SWGRegisterResponseValue* output, NSError* error)) handler;
```

Register new account

Register new account with phone number

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGRegisterParamBody* body = [[SWGRegisterParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Register new account
[apiInstance callRegisterWithAver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGRegisterResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->callRegister: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGRegisterParamBody***](SWGRegisterParamBody*.md)|  | [optional] 

### Return type

[**SWGRegisterResponseValue***](SWGRegisterResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **changePassword**
```objc
-(NSNumber*) changePasswordWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGChangePassParamBody*) body
        completionHandler: (void (^)(SWGChangePassResponseValue* output, NSError* error)) handler;
```

Change password of authenticated user

Change password of authenticated user

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGChangePassParamBody* body = [[SWGChangePassParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Change password of authenticated user
[apiInstance changePasswordWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGChangePassResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->changePassword: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGChangePassParamBody***](SWGChangePassParamBody*.md)|  | [optional] 

### Return type

[**SWGChangePassResponseValue***](SWGChangePassResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **login**
```objc
-(NSNumber*) loginWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGLoginParamBody*) body
        completionHandler: (void (^)(SWGLoginResponseValue* output, NSError* error)) handler;
```

Login with phone & password

Login with phone & password

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGLoginParamBody* body = [[SWGLoginParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Login with phone & password
[apiInstance loginWithAver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGLoginResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->login: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGLoginParamBody***](SWGLoginParamBody*.md)|  | [optional] 

### Return type

[**SWGLoginResponseValue***](SWGLoginResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resetPassword**
```objc
-(NSNumber*) resetPasswordWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGResetPassParamBody*) body
        completionHandler: (void (^)(SWGResetPassResponseValue* output, NSError* error)) handler;
```

Reset password

Reset password

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGResetPassParamBody* body = [[SWGResetPassParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Reset password
[apiInstance resetPasswordWithAver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGResetPassResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->resetPassword: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGResetPassParamBody***](SWGResetPassParamBody*.md)|  | [optional] 

### Return type

[**SWGResetPassResponseValue***](SWGResetPassResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sessionAuthLogin**
```objc
-(NSNumber*) sessionAuthLoginWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGLoginParamBody*) body
        completionHandler: (void (^)(SWGLoginResponseValue* output, NSError* error)) handler;
```

Login with phone & password

Login with phone & password

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGLoginParamBody* body = [[SWGLoginParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Login with phone & password
[apiInstance sessionAuthLoginWithAver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGLoginResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->sessionAuthLogin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGLoginParamBody***](SWGLoginParamBody*.md)|  | [optional] 

### Return type

[**SWGLoginResponseValue***](SWGLoginResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sessionAuthRegister**
```objc
-(NSNumber*) sessionAuthRegisterWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGRegisterParamBody*) body
        completionHandler: (void (^)(SWGRegisterResponseValue* output, NSError* error)) handler;
```

Register new account

Register new account with phone number

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGRegisterParamBody* body = [[SWGRegisterParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Register new account
[apiInstance sessionAuthRegisterWithAver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGRegisterResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->sessionAuthRegister: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGRegisterParamBody***](SWGRegisterParamBody*.md)|  | [optional] 

### Return type

[**SWGRegisterResponseValue***](SWGRegisterResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sessionAuthResetPwd**
```objc
-(NSNumber*) sessionAuthResetPwdWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGResetPassParamBody*) body
        completionHandler: (void (^)(SWGResetPassResponseValue* output, NSError* error)) handler;
```

Reset password

Reset password

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGResetPassParamBody* body = [[SWGResetPassParamBody alloc] init]; //  (optional)

SWGSessionApi*apiInstance = [[SWGSessionApi alloc] init];

// Reset password
[apiInstance sessionAuthResetPwdWithAver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGResetPassResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGSessionApi->sessionAuthResetPwd: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGResetPassParamBody***](SWGResetPassParamBody*.md)|  | [optional] 

### Return type

[**SWGResetPassResponseValue***](SWGResetPassResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

