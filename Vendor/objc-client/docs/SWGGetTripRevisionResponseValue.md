# SWGGetTripRevisionResponseValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | [**SWGResponseError***](SWGResponseError.md) |  | [optional] 
**data** | [**SWGGetTripRevisionResponseValueData***](SWGGetTripRevisionResponseValueData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


