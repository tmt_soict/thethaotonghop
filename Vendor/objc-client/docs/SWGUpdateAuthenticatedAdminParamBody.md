# SWGUpdateAuthenticatedAdminParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dplname** | **NSString*** |  | [optional] 
**avatar** | **NSString*** | avatar url | [optional] 
**phone** | **NSString*** | phone | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


