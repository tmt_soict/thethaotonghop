# SWGAdmin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**uname** | **NSString*** |  | 
**dplname** | **NSString*** |  | 
**phone** | **NSString*** |  | 
**avatar** | **NSString*** | avatar url | 
**permission** | **NSNumber*** |  | 
**operator** | [**SWGOperator***](SWGOperator.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


