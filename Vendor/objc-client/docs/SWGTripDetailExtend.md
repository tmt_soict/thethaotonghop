# SWGTripDetailExtend

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**driverId** | **NSNumber*** |  | 
**startTime** | **NSNumber*** |  | 
**route** | [**NSArray&lt;SWGNewStop&gt;***](SWGNewStop.md) | list stops | 
**distance** | **NSNumber*** |  | 
**income** | **NSNumber*** | total price of trip | 
**totalPassengers** | **NSNumber*** |  | 
**status** | **NSNumber*** | WAITING &#x3D; 0 | RUNNING &#x3D; 10 | FINISHED &#x3D; 15 | CANCELED &#x3D; 16 | DELETED &#x3D; 40 | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


