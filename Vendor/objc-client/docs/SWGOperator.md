# SWGOperator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**name** | **NSString*** |  | 
**desc** | **NSString*** | descriptions info | 
**location** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**logoUrl** | **NSString*** |  | 
**phones** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**operateArea** | [**NSArray&lt;SWGLocationPoint&gt;***](SWGLocationPoint.md) |  | [optional] 
**prefix** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


