# SWGLocationPoint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **NSNumber*** | latitude | 
**lng** | **NSNumber*** | longitude | 
**address** | **NSString*** |  | 
**compactAddress** | **NSString*** |  | [optional] 
**area** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


