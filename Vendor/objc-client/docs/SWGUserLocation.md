# SWGUserLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**time** | **NSNumber*** | epoch time | 
**tag** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


