# SWGBooking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingCode** | **NSString*** |  | [optional] 
**bookingId** | **NSNumber*** |  | [optional] 
**requestId** | **NSNumber*** |  | [optional] 
**refTripStatus** | **NSNumber*** | trip status. 0&#x3D;waiting, 10&#x3D;running, 15&#x3D;finished, 16&#x3D;canceled | 
**passengerId** | **NSNumber*** |  | [optional] 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**pickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**estimatePickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**dropoffTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**load** | **NSNumber*** | number of passengers | 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | 
**status** | **NSNumber*** | booking status | 0 &#x3D; PENDING | 1 &#x3D; PREPROCESSED | 2 &#x3D; PROCESSED | 16 &#x3D; CANCELED | 
**distance** | **NSNumber*** |  | 
**priceRange** | [**SWGCostRange***](SWGCostRange.md) |  | 
**vehicleQuality** | **NSNumber*** | car&#39;s quality NONE &#x3D; 0 | ECO &#x3D; 1 | HIGH &#x3D; 2 | LUX &#x3D; 4 | [optional] 
**vehicleType** | **NSNumber*** | car&#39;s capacity 4, 7, 9, 16 .. | [optional] 
**operatorId** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


