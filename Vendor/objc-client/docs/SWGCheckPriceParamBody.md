# SWGCheckPriceParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance** | **NSNumber*** |  | [optional] 
**load** | **NSNumber*** |  | [optional] 
**time** | **NSNumber*** |  | [optional] 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | [optional] 
**vehicleQuality** | **NSNumber*** | car&#39;s quality ECO &#x3D; 1 | 6 &#x3D; HIGH &amp; LUXURY | [optional] [default to @1]
**vehicleType** | **NSNumber*** | car&#39;s capacity 4, 7, 9, 16 .. | [optional] [default to @4]
**operatorId** | **NSNumber*** |  | [optional] [default to @0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


