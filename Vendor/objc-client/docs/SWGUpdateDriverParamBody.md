# SWGUpdateDriverParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dplname** | **NSString*** | Display name | [optional] 
**avatar** | **NSString*** | Link hình ảnh avatar | [optional] 
**defaultLocation** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**vehicleType** | **NSNumber*** | loại xe 4 chỗ, 7 chỗ, 16 chỗ | [optional] 
**licensePlate** | **NSString*** | biển số xe. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


