# SWGChangePassParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uname** | **NSString*** | username / phone | 
**oldPassword** | **NSString*** |  | 
**varNewPassword** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


