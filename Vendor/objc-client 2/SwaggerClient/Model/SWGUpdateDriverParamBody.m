#import "SWGUpdateDriverParamBody.h"

@implementation SWGUpdateDriverParamBody

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"dplname": @"dplname", @"avatar": @"avatar", @"default_location": @"defaultLocation", @"vehicle_type": @"vehicleType", @"license_plate": @"licensePlate" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"dplname", @"avatar", @"defaultLocation", @"vehicleType", @"licensePlate"];
  return [optionalProperties containsObject:propertyName];
}

@end
