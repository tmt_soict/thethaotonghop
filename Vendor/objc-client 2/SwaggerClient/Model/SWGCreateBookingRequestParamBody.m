#import "SWGCreateBookingRequestParamBody.h"

@implementation SWGCreateBookingRequestParamBody

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.operatorId = @0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"from": @"from", @"to": @"to", @"pickup_time": @"pickupTime", @"dropoff_time": @"dropoffTime", @"vehicle_quality": @"vehicleQuality", @"vehicle_type": @"vehicleType", @"load": @"load", @"is_sharing": @"isSharing", @"status": @"status", @"distance": @"distance", @"price_range": @"priceRange", @"operator_id": @"operatorId" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"from", @"to", @"pickupTime", @"dropoffTime", @"vehicleQuality", @"vehicleType", @"load", @"isSharing", @"status", @"distance", @"priceRange", @"operatorId"];
  return [optionalProperties containsObject:propertyName];
}

@end
