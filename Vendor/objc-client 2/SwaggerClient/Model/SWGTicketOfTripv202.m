#import "SWGTicketOfTripv202.h"

@implementation SWGTicketOfTripv202

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"booking_id": @"bookingId", @"booking_code": @"bookingCode", @"passenger": @"passenger", @"start_time": @"startTime", @"from_address": @"fromAddress", @"to_address": @"toAddress", @"number_passenger": @"numberPassenger", @"price": @"price", @"is_sharing": @"isSharing", @"booking_status": @"bookingStatus" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"passenger", ];
  return [optionalProperties containsObject:propertyName];
}

@end
