#import "SWGDeviceApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGRegisterDeviceResponseValue.h"
#import "SWGDevice.h"
#import "SWGRemoveDeviceResponseValue.h"


@interface SWGDeviceApi ()

@property (nonatomic, strong) NSMutableDictionary *defaultHeaders;

@end

@implementation SWGDeviceApi

NSString* kSWGDeviceApiErrorDomain = @"SWGDeviceApiErrorDomain";
NSInteger kSWGDeviceApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    self = [super init];
    if (self) {
        SWGConfiguration *config = [SWGConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[SWGApiClient alloc] init];
        }
        _apiClient = config.apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+ (instancetype)sharedAPI {
    static SWGDeviceApi *sharedAPI;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.defaultHeaders[key];
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self setDefaultHeaderValue:value forKey:key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(NSUInteger) requestQueueSize {
    return [SWGApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Register device
/// Register device
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGRegisterDeviceResponseValue*
///
-(NSNumber*) registerDeviceWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGDevice*) body
    completionHandler: (void (^)(SWGRegisterDeviceResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/devices"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGRegisterDeviceResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGRegisterDeviceResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Remove device.
/// Remove device
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGRemoveDeviceResponseValue*
///
-(NSNumber*) removeDeviceWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGDevice*) body
    completionHandler: (void (^)(SWGRemoveDeviceResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/devices"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PUT"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGRemoveDeviceResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGRemoveDeviceResponseValue*)data, error);
                                }
                           }
          ];
}



@end
