# SWGTripRevision

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**editor** | **NSNumber*** |  | 
**time** | **NSNumber*** |  | 
**tripInfo** | [**SWGTripInfo***](SWGTripInfo.md) |  | [optional] 
**tripDetail** | [**SWGTripDetailExtend***](SWGTripDetailExtend.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


