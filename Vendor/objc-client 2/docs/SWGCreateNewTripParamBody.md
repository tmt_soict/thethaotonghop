# SWGCreateNewTripParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **NSNumber*** |  | [optional] 
**startTime** | **NSNumber*** |  | [optional] 
**distance** | **NSNumber*** |  | [optional] 
**status** | **NSNumber*** |  | [optional] 
**route** | [**NSArray&lt;SWGStopInfo&gt;***](SWGStopInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


