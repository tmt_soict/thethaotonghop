# SWGGetAllAdminsResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**admins** | [**NSArray&lt;SWGAdmin&gt;***](SWGAdmin.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


