# SWGTripApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dropoffPassenger**](SWGTripApi.md#dropoffpassenger) | **POST** /trips/{trip_id}/dropoff/{booking_id} | Dropoff a passenger
[**getListPassengersOfTrip**](SWGTripApi.md#getlistpassengersoftrip) | **GET** /trips/{trip_id}/passengers | Get list passengers of the trip
[**getListPassengersOfTripv201**](SWGTripApi.md#getlistpassengersoftripv201) | **GET** /trips/{trip_id}/passengers/v2_0_1 | Get list passengers of the trip
[**getListPassengersOfTripv202**](SWGTripApi.md#getlistpassengersoftripv202) | **GET** /trips/{trip_id}/passengers/v2_0_2 | Get list passengers of the trip
[**getTrip**](SWGTripApi.md#gettrip) | **GET** /trips/{trip_id} | Get a single trip
[**getTripv201**](SWGTripApi.md#gettripv201) | **GET** /trips/{trip_id}/v2_0_1 | Get a single trip
[**pickupPassenger**](SWGTripApi.md#pickuppassenger) | **POST** /trips/{trip_id}/pickup/{booking_id} | Pickup a passenger
[**updateTrip**](SWGTripApi.md#updatetrip) | **PUT** /trips/{trip_id} | Update trip
[**updateTripStatus**](SWGTripApi.md#updatetripstatus) | **POST** /trips/{trip_id}/status/{trip_status} | Update trip status


# **dropoffPassenger**
```objc
-(NSNumber*) dropoffPassengerWithTripId: (NSNumber*) tripId
    bookingId: (NSNumber*) bookingId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGDropoffPassengerResponseValue* output, NSError* error)) handler;
```

Dropoff a passenger

Dropoff a passenger

### Example 
```objc

NSNumber* tripId = @789; // 
NSNumber* bookingId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Dropoff a passenger
[apiInstance dropoffPassengerWithTripId:tripId
              bookingId:bookingId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGDropoffPassengerResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->dropoffPassenger: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **bookingId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGDropoffPassengerResponseValue***](SWGDropoffPassengerResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getListPassengersOfTrip**
```objc
-(NSNumber*) getListPassengersOfTripWithTripId: (NSNumber*) tripId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetListPassengerOfTripResponseValue* output, NSError* error)) handler;
```

Get list passengers of the trip

Get list passengers of the trip

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Get list passengers of the trip
[apiInstance getListPassengersOfTripWithTripId:tripId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetListPassengerOfTripResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->getListPassengersOfTrip: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetListPassengerOfTripResponseValue***](SWGGetListPassengerOfTripResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getListPassengersOfTripv201**
```objc
-(NSNumber*) getListPassengersOfTripv201WithTripId: (NSNumber*) tripId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetListPassengerOfTripv201ResponseValue* output, NSError* error)) handler;
```

Get list passengers of the trip

Get list passengers of the trip

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Get list passengers of the trip
[apiInstance getListPassengersOfTripv201WithTripId:tripId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetListPassengerOfTripv201ResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->getListPassengersOfTripv201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetListPassengerOfTripv201ResponseValue***](SWGGetListPassengerOfTripv201ResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getListPassengersOfTripv202**
```objc
-(NSNumber*) getListPassengersOfTripv202WithTripId: (NSNumber*) tripId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetListPassengerOfTripv202ResponseValue* output, NSError* error)) handler;
```

Get list passengers of the trip

Get list passengers of the trip

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Get list passengers of the trip
[apiInstance getListPassengersOfTripv202WithTripId:tripId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetListPassengerOfTripv202ResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->getListPassengersOfTripv202: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetListPassengerOfTripv202ResponseValue***](SWGGetListPassengerOfTripv202ResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTrip**
```objc
-(NSNumber*) getTripWithTripId: (NSNumber*) tripId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetTripResponseValue* output, NSError* error)) handler;
```

Get a single trip

Get a songle trip

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Get a single trip
[apiInstance getTripWithTripId:tripId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetTripResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->getTrip: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetTripResponseValue***](SWGGetTripResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTripv201**
```objc
-(NSNumber*) getTripv201WithTripId: (NSNumber*) tripId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetTripv201ResponseValue* output, NSError* error)) handler;
```

Get a single trip

Get a single trip

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Get a single trip
[apiInstance getTripv201WithTripId:tripId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetTripv201ResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->getTripv201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetTripv201ResponseValue***](SWGGetTripv201ResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pickupPassenger**
```objc
-(NSNumber*) pickupPassengerWithTripId: (NSNumber*) tripId
    bookingId: (NSNumber*) bookingId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGPickupPassengerResponseValue* output, NSError* error)) handler;
```

Pickup a passenger

Pickup a passenger

### Example 
```objc

NSNumber* tripId = @789; // 
NSNumber* bookingId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Pickup a passenger
[apiInstance pickupPassengerWithTripId:tripId
              bookingId:bookingId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGPickupPassengerResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->pickupPassenger: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **bookingId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGPickupPassengerResponseValue***](SWGPickupPassengerResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTrip**
```objc
-(NSNumber*) updateTripWithTripId: (NSNumber*) tripId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGUpdateTripParamBody*) body
        completionHandler: (void (^)(SWGUpdateTripResponseValue* output, NSError* error)) handler;
```

Update trip

Update trip

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGUpdateTripParamBody* body = [[SWGUpdateTripParamBody alloc] init]; //  (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Update trip
[apiInstance updateTripWithTripId:tripId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGUpdateTripResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->updateTrip: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGUpdateTripParamBody***](SWGUpdateTripParamBody*.md)|  | [optional] 

### Return type

[**SWGUpdateTripResponseValue***](SWGUpdateTripResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTripStatus**
```objc
-(NSNumber*) updateTripStatusWithTripId: (NSNumber*) tripId
    tripStatus: (NSNumber*) tripStatus
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGUpdateTripStatusResponseValue* output, NSError* error)) handler;
```

Update trip status

Update trip status

### Example 
```objc

NSNumber* tripId = @789; // 
NSNumber* tripStatus = @789; // WAITING = 0 | RUNNING = 10 | FINISHED = 15 | CANCELED = 16;
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGTripApi*apiInstance = [[SWGTripApi alloc] init];

// Update trip status
[apiInstance updateTripStatusWithTripId:tripId
              tripStatus:tripStatus
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGUpdateTripStatusResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGTripApi->updateTripStatus: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **tripStatus** | **NSNumber***| WAITING &#x3D; 0 | RUNNING &#x3D; 10 | FINISHED &#x3D; 15 | CANCELED &#x3D; 16; | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGUpdateTripStatusResponseValue***](SWGUpdateTripStatusResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

