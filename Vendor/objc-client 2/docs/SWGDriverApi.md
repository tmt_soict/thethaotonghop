# SWGDriverApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**driverReadNotification**](SWGDriverApi.md#driverreadnotification) | **POST** /driver/notifications/read/{notification_id} | read a notidication
[**getAuthenticatedDriver**](SWGDriverApi.md#getauthenticateddriver) | **GET** /driver | Get the authenticated driver
[**getCurrentTrip**](SWGDriverApi.md#getcurrenttrip) | **GET** /driver/trips/current | Get current trip
[**getDriverNotifications**](SWGDriverApi.md#getdrivernotifications) | **GET** /driver/notifications | Get notifications
[**getDriverStatsById**](SWGDriverApi.md#getdriverstatsbyid) | **GET** /driver/{driver_id}/stats | Get statistic of driver by id
[**getPageTrips**](SWGDriverApi.md#getpagetrips) | **GET** /driver/trips | Get page trips of authenticated driver
[**getSchedules**](SWGDriverApi.md#getschedules) | **GET** /driver/schedules | Get schedules from one day to the end of month
[**getTripHistory**](SWGDriverApi.md#gettriphistory) | **GET** /driver/trip_history | Get page trips of authenticated driver
[**getTripsByDate**](SWGDriverApi.md#gettripsbydate) | **GET** /driver/trips/{selected_date} | Get trips by date of authenticated driver
[**processNotify**](SWGDriverApi.md#processnotify) | **POST** /driver/notifications | Get page trips of authenticated driver
[**setDriverStats**](SWGDriverApi.md#setdriverstats) | **POST** /driver/stats | Set statistic of driver
[**setSchedule**](SWGDriverApi.md#setschedule) | **POST** /driver/schedules | Set schedule of one day
[**setSchedulev201**](SWGDriverApi.md#setschedulev201) | **POST** /driver/schedules/v2_0_1 | Set schedule of day(s)
[**updateAuthenticatedDriver**](SWGDriverApi.md#updateauthenticateddriver) | **PUT** /driver | Update authenticated driver


# **driverReadNotification**
```objc
-(NSNumber*) driverReadNotificationWithNotificationId: (NSNumber*) notificationId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGReadNotificationResponseValue* output, NSError* error)) handler;
```

read a notidication

read a notidication

### Example 
```objc

NSNumber* notificationId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// read a notidication
[apiInstance driverReadNotificationWithNotificationId:notificationId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGReadNotificationResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->driverReadNotification: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notificationId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGReadNotificationResponseValue***](SWGReadNotificationResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAuthenticatedDriver**
```objc
-(NSNumber*) getAuthenticatedDriverWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetAuthenticatedDriverResponseValue* output, NSError* error)) handler;
```

Get the authenticated driver

Get the authenticated driver

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get the authenticated driver
[apiInstance getAuthenticatedDriverWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetAuthenticatedDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getAuthenticatedDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetAuthenticatedDriverResponseValue***](SWGGetAuthenticatedDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getCurrentTrip**
```objc
-(NSNumber*) getCurrentTripWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetTripv201ResponseValue* output, NSError* error)) handler;
```

Get current trip

Get current trip

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get current trip
[apiInstance getCurrentTripWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetTripv201ResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getCurrentTrip: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetTripv201ResponseValue***](SWGGetTripv201ResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getDriverNotifications**
```objc
-(NSNumber*) getDriverNotificationsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetNotificationsResponseValue* output, NSError* error)) handler;
```

Get notifications

Get notifications

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get notifications
[apiInstance getDriverNotificationsWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetNotificationsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getDriverNotifications: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetNotificationsResponseValue***](SWGGetNotificationsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getDriverStatsById**
```objc
-(NSNumber*) getDriverStatsByIdWithDriverId: (NSNumber*) driverId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetDriverStatsResponseValue* output, NSError* error)) handler;
```

Get statistic of driver by id

Get statistic of driver by id

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 =GroupAdmin client | 100 = supper admin (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get statistic of driver by id
[apiInstance getDriverStatsByIdWithDriverId:driverId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetDriverStatsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getDriverStatsById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D;GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetDriverStatsResponseValue***](SWGGetDriverStatsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPageTrips**
```objc
-(NSNumber*) getPageTripsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastid: (NSNumber*) lastid
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
        completionHandler: (void (^)(SWGGetPageTripResponseValue* output, NSError* error)) handler;
```

Get page trips of authenticated driver

Get page trips of authenticated driver

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* lastid = @789; // id of last item / last date (optional)
NSNumber* minsize = @56; // minimum items / request (optional)
NSNumber* ordertype = @56; // 0 = old -> new | 1 = new -> old (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get page trips of authenticated driver
[apiInstance getPageTripsWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              lastid:lastid
              minsize:minsize
              ordertype:ordertype
          completionHandler: ^(SWGGetPageTripResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getPageTrips: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **lastid** | **NSNumber***| id of last item / last date | [optional] 
 **minsize** | **NSNumber***| minimum items / request | [optional] 
 **ordertype** | **NSNumber***| 0 &#x3D; old -&gt; new | 1 &#x3D; new -&gt; old | [optional] 

### Return type

[**SWGGetPageTripResponseValue***](SWGGetPageTripResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getSchedules**
```objc
-(NSNumber*) getSchedulesWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    fromdate: (NSNumber*) fromdate
    todate: (NSNumber*) todate
        completionHandler: (void (^)(SWGGetScheduleResponseValue* output, NSError* error)) handler;
```

Get schedules from one day to the end of month

Get schedules from one day to the end of month

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* fromdate = @789; // epoch time, default value is current time (optional)
NSNumber* todate = @789; // epoch time, default value is current time (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get schedules from one day to the end of month
[apiInstance getSchedulesWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              fromdate:fromdate
              todate:todate
          completionHandler: ^(SWGGetScheduleResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getSchedules: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **fromdate** | **NSNumber***| epoch time, default value is current time | [optional] 
 **todate** | **NSNumber***| epoch time, default value is current time | [optional] 

### Return type

[**SWGGetScheduleResponseValue***](SWGGetScheduleResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTripHistory**
```objc
-(NSNumber*) getTripHistoryWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastid: (NSNumber*) lastid
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
        completionHandler: (void (^)(SWGGetTripHistoryResponseValue* output, NSError* error)) handler;
```

Get page trips of authenticated driver

Get page trips of authenticated driver

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* lastid = @789; // id of last item / last date (optional)
NSNumber* minsize = @56; // minimum items / request (optional)
NSNumber* ordertype = @56; // 0 = old -> new | 1 = new -> old (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get page trips of authenticated driver
[apiInstance getTripHistoryWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              lastid:lastid
              minsize:minsize
              ordertype:ordertype
          completionHandler: ^(SWGGetTripHistoryResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getTripHistory: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **lastid** | **NSNumber***| id of last item / last date | [optional] 
 **minsize** | **NSNumber***| minimum items / request | [optional] 
 **ordertype** | **NSNumber***| 0 &#x3D; old -&gt; new | 1 &#x3D; new -&gt; old | [optional] 

### Return type

[**SWGGetTripHistoryResponseValue***](SWGGetTripHistoryResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTripsByDate**
```objc
-(NSNumber*) getTripsByDateWithSelectedDate: (NSNumber*) selectedDate
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetTripByDateResponseValue* output, NSError* error)) handler;
```

Get trips by date of authenticated driver

Get trips by date of authenticated driver

### Example 
```objc

NSNumber* selectedDate = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get trips by date of authenticated driver
[apiInstance getTripsByDateWithSelectedDate:selectedDate
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetTripByDateResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->getTripsByDate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selectedDate** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetTripByDateResponseValue***](SWGGetTripByDateResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **processNotify**
```objc
-(NSNumber*) processNotifyWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGProcessNotifyParamBody*) body
        completionHandler: (void (^)(SWGProcessNotifyResponseValue* output, NSError* error)) handler;
```

Get page trips of authenticated driver

Get page trips of authenticated driver

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGProcessNotifyParamBody* body = [[SWGProcessNotifyParamBody alloc] init]; //  (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Get page trips of authenticated driver
[apiInstance processNotifyWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGProcessNotifyResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->processNotify: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGProcessNotifyParamBody***](SWGProcessNotifyParamBody*.md)|  | [optional] 

### Return type

[**SWGProcessNotifyResponseValue***](SWGProcessNotifyResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setDriverStats**
```objc
-(NSNumber*) setDriverStatsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSetDriverStatsParamBody*) body
        completionHandler: (void (^)(SWGSetDriverStatsResponseValue* output, NSError* error)) handler;
```

Set statistic of driver

Set statistic of driver

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 =GroupAdmin client | 100 = supper admin (optional)
SWGSetDriverStatsParamBody* body = [[SWGSetDriverStatsParamBody alloc] init]; //  (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Set statistic of driver
[apiInstance setDriverStatsWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGSetDriverStatsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->setDriverStats: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D;GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGSetDriverStatsParamBody***](SWGSetDriverStatsParamBody*.md)|  | [optional] 

### Return type

[**SWGSetDriverStatsResponseValue***](SWGSetDriverStatsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setSchedule**
```objc
-(NSNumber*) setScheduleWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSchedule*) body
        completionHandler: (void (^)(SWGSetScheduleResponseValue* output, NSError* error)) handler;
```

Set schedule of one day

Set schedule of one day

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGSchedule* body = [[SWGSchedule alloc] init]; //  (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Set schedule of one day
[apiInstance setScheduleWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGSetScheduleResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->setSchedule: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGSchedule***](SWGSchedule*.md)|  | [optional] 

### Return type

[**SWGSetScheduleResponseValue***](SWGSetScheduleResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setSchedulev201**
```objc
-(NSNumber*) setSchedulev201WithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSchedules*) body
        completionHandler: (void (^)(SWGSetSchedulesResponseValue* output, NSError* error)) handler;
```

Set schedule of day(s)

Set schedule of day(s)

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGSchedules* body = [[SWGSchedules alloc] init]; //  (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Set schedule of day(s)
[apiInstance setSchedulev201WithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGSetSchedulesResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->setSchedulev201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGSchedules***](SWGSchedules*.md)|  | [optional] 

### Return type

[**SWGSetSchedulesResponseValue***](SWGSetSchedulesResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateAuthenticatedDriver**
```objc
-(NSNumber*) updateAuthenticatedDriverWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGUpdateAuthenticatedDriverParamBody*) body
        completionHandler: (void (^)(SWGUpdateAuthenticatedDriverResponseValue* output, NSError* error)) handler;
```

Update authenticated driver

Update authenticated driver

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGUpdateAuthenticatedDriverParamBody* body = [[SWGUpdateAuthenticatedDriverParamBody alloc] init]; //  (optional)

SWGDriverApi*apiInstance = [[SWGDriverApi alloc] init];

// Update authenticated driver
[apiInstance updateAuthenticatedDriverWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGUpdateAuthenticatedDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDriverApi->updateAuthenticatedDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGUpdateAuthenticatedDriverParamBody***](SWGUpdateAuthenticatedDriverParamBody*.md)|  | [optional] 

### Return type

[**SWGUpdateAuthenticatedDriverResponseValue***](SWGUpdateAuthenticatedDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

