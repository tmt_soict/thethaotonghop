# SWGGetTripRevisionByTimeResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tripRevision** | [**SWGTripRevision***](SWGTripRevision.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


