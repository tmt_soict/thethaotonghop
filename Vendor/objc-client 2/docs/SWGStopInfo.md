# SWGStopInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **NSNumber*** |  | 
**location** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**arriveTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**stopType** | **NSNumber*** | 1 &#x3D; pickup | 2 &#x3D; dropoff | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


