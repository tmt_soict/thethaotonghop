# SWGTimeRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **NSNumber*** | epoch time | 
**endTime** | **NSNumber*** | epoch time | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


