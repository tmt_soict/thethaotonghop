# SWGVehicleModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**quality** | **NSNumber*** | NONE &#x3D; 0 | ECO &#x3D; 1 | HIGH &#x3D; 2 | LUXURY &#x3D; 4 | [optional] 
**year** | **NSNumber*** |  | [optional] 
**capacity** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


