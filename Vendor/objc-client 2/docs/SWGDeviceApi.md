# SWGDeviceApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**registerDevice**](SWGDeviceApi.md#registerdevice) | **POST** /devices | Register device
[**removeDevice**](SWGDeviceApi.md#removedevice) | **PUT** /devices | Remove device.


# **registerDevice**
```objc
-(NSNumber*) registerDeviceWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGDevice*) body
        completionHandler: (void (^)(SWGRegisterDeviceResponseValue* output, NSError* error)) handler;
```

Register device

Register device

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGDevice* body = [[SWGDevice alloc] init]; //  (optional)

SWGDeviceApi*apiInstance = [[SWGDeviceApi alloc] init];

// Register device
[apiInstance registerDeviceWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGRegisterDeviceResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDeviceApi->registerDevice: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGDevice***](SWGDevice*.md)|  | [optional] 

### Return type

[**SWGRegisterDeviceResponseValue***](SWGRegisterDeviceResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **removeDevice**
```objc
-(NSNumber*) removeDeviceWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGDevice*) body
        completionHandler: (void (^)(SWGRemoveDeviceResponseValue* output, NSError* error)) handler;
```

Remove device.

Remove device

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGDevice* body = [[SWGDevice alloc] init]; //  (optional)

SWGDeviceApi*apiInstance = [[SWGDeviceApi alloc] init];

// Remove device.
[apiInstance removeDeviceWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGRemoveDeviceResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGDeviceApi->removeDevice: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGDevice***](SWGDevice*.md)|  | [optional] 

### Return type

[**SWGRemoveDeviceResponseValue***](SWGRemoveDeviceResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

