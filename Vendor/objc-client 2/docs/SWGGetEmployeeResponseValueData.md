# SWGGetEmployeeResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**admins** | [**NSArray&lt;SWGEmployee&gt;***](SWGEmployee.md) |  | 
**drivers** | [**NSArray&lt;SWGEmployee&gt;***](SWGEmployee.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


