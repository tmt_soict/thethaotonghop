# SWGProcessNotifyParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifyId** | **NSNumber*** |  | [optional] 
**actionType** | **NSNumber*** | ACCEPT &#x3D; 1 | REJECT &#x3D; 2 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


