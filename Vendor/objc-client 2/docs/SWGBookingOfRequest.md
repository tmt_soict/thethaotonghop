# SWGBookingOfRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingId** | **NSNumber*** |  | 
**bookingCode** | **NSString*** |  | 
**passenger** | [**SWGPassenger***](SWGPassenger.md) |  | 
**load** | **NSNumber*** | number of passengers | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


