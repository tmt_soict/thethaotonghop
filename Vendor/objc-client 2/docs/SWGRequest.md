# SWGRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**requestId** | **NSNumber*** |  | 
**pickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**dropoffTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**numberPassenger** | **NSNumber*** | number of passengers | 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | 
**requestStatus** | **NSNumber*** |  | 
**distance** | **NSNumber*** |  | [optional] 
**priceSum** | **NSNumber*** |  | 
**bookings** | [**NSArray&lt;SWGBookingOfRequest&gt;***](SWGBookingOfRequest.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


