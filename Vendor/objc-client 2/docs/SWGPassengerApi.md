# SWGPassengerApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**checkPrice**](SWGPassengerApi.md#checkprice) | **POST** /passenger/bookings/checkprice | Check price before make booking
[**createBookingRequest**](SWGPassengerApi.md#createbookingrequest) | **POST** /passenger/bookings | Make new booking request
[**getAuthenticatedPassenger**](SWGPassengerApi.md#getauthenticatedpassenger) | **GET** /passenger | Get the authenticated passenger
[**getBookingHistory**](SWGPassengerApi.md#getbookinghistory) | **GET** /passenger/booking_history | Get booking history
[**getPageBookings**](SWGPassengerApi.md#getpagebookings) | **GET** /passenger/bookings | Get page bookings
[**getPassengerNotifications**](SWGPassengerApi.md#getpassengernotifications) | **GET** /passenger/notifications | Get notifications
[**passengerReadNotification**](SWGPassengerApi.md#passengerreadnotification) | **POST** /passenger/notifications/read/{notification_id} | read a notidication
[**updateAuthenticatedPassenger**](SWGPassengerApi.md#updateauthenticatedpassenger) | **PUT** /passenger | Update authenticated passenger


# **checkPrice**
```objc
-(NSNumber*) checkPriceWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGCheckPriceParamBody*) body
        completionHandler: (void (^)(SWGCheckPriceResponseValue* output, NSError* error)) handler;
```

Check price before make booking

Check price before make booking

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGCheckPriceParamBody* body = [[SWGCheckPriceParamBody alloc] init]; //  (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Check price before make booking
[apiInstance checkPriceWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGCheckPriceResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->checkPrice: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGCheckPriceParamBody***](SWGCheckPriceParamBody*.md)|  | [optional] 

### Return type

[**SWGCheckPriceResponseValue***](SWGCheckPriceResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createBookingRequest**
```objc
-(NSNumber*) createBookingRequestWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGCreateBookingRequestParamBody*) body
        completionHandler: (void (^)(SWGCreateBookingRequestResponseValue* output, NSError* error)) handler;
```

Make new booking request

Make new booking requests

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGCreateBookingRequestParamBody* body = [[SWGCreateBookingRequestParamBody alloc] init]; //  (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Make new booking request
[apiInstance createBookingRequestWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGCreateBookingRequestResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->createBookingRequest: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGCreateBookingRequestParamBody***](SWGCreateBookingRequestParamBody*.md)|  | [optional] 

### Return type

[**SWGCreateBookingRequestResponseValue***](SWGCreateBookingRequestResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAuthenticatedPassenger**
```objc
-(NSNumber*) getAuthenticatedPassengerWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetAuthenticatedPassengerResponseValue* output, NSError* error)) handler;
```

Get the authenticated passenger

Get the authenticated passenger

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Get the authenticated passenger
[apiInstance getAuthenticatedPassengerWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetAuthenticatedPassengerResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->getAuthenticatedPassenger: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetAuthenticatedPassengerResponseValue***](SWGGetAuthenticatedPassengerResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getBookingHistory**
```objc
-(NSNumber*) getBookingHistoryWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastid: (NSNumber*) lastid
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
        completionHandler: (void (^)(SWGGetBookingHistoryResponseValue* output, NSError* error)) handler;
```

Get booking history

Get booking history

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* lastid = @789; // id of last item / last date (optional)
NSNumber* minsize = @56; // minimum items / request (optional)
NSNumber* ordertype = @56; // 0 = order by created time new -> old | 1 = order bycreated time old -> new | 2 = order by start time new -> old | 3 = order by start time old -> new (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Get booking history
[apiInstance getBookingHistoryWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              lastid:lastid
              minsize:minsize
              ordertype:ordertype
          completionHandler: ^(SWGGetBookingHistoryResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->getBookingHistory: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **lastid** | **NSNumber***| id of last item / last date | [optional] 
 **minsize** | **NSNumber***| minimum items / request | [optional] 
 **ordertype** | **NSNumber***| 0 &#x3D; order by created time new -&gt; old | 1 &#x3D; order bycreated time old -&gt; new | 2 &#x3D; order by start time new -&gt; old | 3 &#x3D; order by start time old -&gt; new | [optional] 

### Return type

[**SWGGetBookingHistoryResponseValue***](SWGGetBookingHistoryResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPageBookings**
```objc
-(NSNumber*) getPageBookingsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastid: (NSNumber*) lastid
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
        completionHandler: (void (^)(SWGGetPageBookingResponseValue* output, NSError* error)) handler;
```

Get page bookings

Get page bookings

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* lastid = @789; // id of last item / last date (optional)
NSNumber* minsize = @56; // minimum items / request (optional)
NSNumber* ordertype = @56; // 0 = order by created time new -> old | 1 = order bycreated time old -> new | 2 = order by start time new -> old | 3 = order by start time old -> new (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Get page bookings
[apiInstance getPageBookingsWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              lastid:lastid
              minsize:minsize
              ordertype:ordertype
          completionHandler: ^(SWGGetPageBookingResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->getPageBookings: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **lastid** | **NSNumber***| id of last item / last date | [optional] 
 **minsize** | **NSNumber***| minimum items / request | [optional] 
 **ordertype** | **NSNumber***| 0 &#x3D; order by created time new -&gt; old | 1 &#x3D; order bycreated time old -&gt; new | 2 &#x3D; order by start time new -&gt; old | 3 &#x3D; order by start time old -&gt; new | [optional] 

### Return type

[**SWGGetPageBookingResponseValue***](SWGGetPageBookingResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPassengerNotifications**
```objc
-(NSNumber*) getPassengerNotificationsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetNotificationsResponseValue* output, NSError* error)) handler;
```

Get notifications

Get notifications

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Get notifications
[apiInstance getPassengerNotificationsWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetNotificationsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->getPassengerNotifications: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetNotificationsResponseValue***](SWGGetNotificationsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **passengerReadNotification**
```objc
-(NSNumber*) passengerReadNotificationWithNotificationId: (NSNumber*) notificationId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGReadNotificationResponseValue* output, NSError* error)) handler;
```

read a notidication

read a notidication

### Example 
```objc

NSNumber* notificationId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// read a notidication
[apiInstance passengerReadNotificationWithNotificationId:notificationId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGReadNotificationResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->passengerReadNotification: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notificationId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGReadNotificationResponseValue***](SWGReadNotificationResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateAuthenticatedPassenger**
```objc
-(NSNumber*) updateAuthenticatedPassengerWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGUpdateAuthenticatedPassengerParamBody*) body
        completionHandler: (void (^)(SWGUpdateAuthenticatedPassengerResponseValue* output, NSError* error)) handler;
```

Update authenticated passenger

Update authenticated passenger

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGUpdateAuthenticatedPassengerParamBody* body = [[SWGUpdateAuthenticatedPassengerParamBody alloc] init]; //  (optional)

SWGPassengerApi*apiInstance = [[SWGPassengerApi alloc] init];

// Update authenticated passenger
[apiInstance updateAuthenticatedPassengerWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGUpdateAuthenticatedPassengerResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGPassengerApi->updateAuthenticatedPassenger: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGUpdateAuthenticatedPassengerParamBody***](SWGUpdateAuthenticatedPassengerParamBody*.md)|  | [optional] 

### Return type

[**SWGUpdateAuthenticatedPassengerResponseValue***](SWGUpdateAuthenticatedPassengerResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

