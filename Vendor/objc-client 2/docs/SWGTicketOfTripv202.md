# SWGTicketOfTripv202

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingId** | **NSNumber*** |  | 
**bookingCode** | **NSString*** |  | 
**passenger** | [**SWGPassenger***](SWGPassenger.md) |  | [optional] 
**startTime** | **NSNumber*** |  | 
**fromAddress** | **NSString*** |  | 
**toAddress** | **NSString*** |  | 
**numberPassenger** | **NSNumber*** |  | 
**price** | [**SWGCostRange***](SWGCostRange.md) |  | 
**isSharing** | **NSNumber*** |  | 
**bookingStatus** | **NSNumber*** | booking status 0 &#x3D; PENDING| 10 &#x3D; RUNNING | 15 &#x3D; COMPLETED | 16 &#x3D; CANCELED | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


