# SWGGetNotificationsResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notifications** | [**NSArray&lt;SWGNotify&gt;***](SWGNotify.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


