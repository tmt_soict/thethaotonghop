# SWGGetAllRequestsOfDatev201ResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requests** | [**NSArray&lt;SWGRequest&gt;***](SWGRequest.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


