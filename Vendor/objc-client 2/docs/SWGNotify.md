# SWGNotify

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**sender** | **NSNumber*** |  | 
**receiver** | **NSNumber*** |  | 
**status** | **NSNumber*** | UNREAD &#x3D; 0 | READ &#x3D; 1 | 
**type** | **NSNumber*** | MESSAGE &#x3D; 0 | HIRING_INVITATION &#x3D; 1 | TRIP_INVITATION &#x3D; 2 | TRIP_HAVE_DRIVER &#x3D; 3 | TRIP_UPDATE4DRIVER &#x3D; 4 | TRIP_UPDATE4PASSENGER &#x3D; 5 | BOOKING &#x3D; 6 | SYSTEM_MESSAGE &#x3D; 99 | 
**title** | **NSString*** |  | 
**shortContent** | **NSString*** |  | 
**message** | **NSString*** |  | 
**dataId** | **NSNumber*** |  | 
**creationTime** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


