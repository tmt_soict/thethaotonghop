# SWGLocationApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserLocationByUID**](SWGLocationApi.md#deleteuserlocationbyuid) | **DELETE** /locations/{user_id} | Delete user&#39;s location by uid
[**getUserLocationByUID**](SWGLocationApi.md#getuserlocationbyuid) | **GET** /locations/{user_id} | Get user&#39;s location by uid
[**setUserLocation**](SWGLocationApi.md#setuserlocation) | **POST** /locations | User set location


# **deleteUserLocationByUID**
```objc
-(NSNumber*) deleteUserLocationByUIDWithUserId: (NSNumber*) userId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGDeleteUserLocationResponseValue* output, NSError* error)) handler;
```

Delete user's location by uid

Delete user's location by uid

### Example 
```objc

NSNumber* userId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGLocationApi*apiInstance = [[SWGLocationApi alloc] init];

// Delete user's location by uid
[apiInstance deleteUserLocationByUIDWithUserId:userId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGDeleteUserLocationResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGLocationApi->deleteUserLocationByUID: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGDeleteUserLocationResponseValue***](SWGDeleteUserLocationResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getUserLocationByUID**
```objc
-(NSNumber*) getUserLocationByUIDWithUserId: (NSNumber*) userId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetUserLocationResponseValue* output, NSError* error)) handler;
```

Get user's location by uid

Get user's location by uid

### Example 
```objc

NSNumber* userId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGLocationApi*apiInstance = [[SWGLocationApi alloc] init];

// Get user's location by uid
[apiInstance getUserLocationByUIDWithUserId:userId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetUserLocationResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGLocationApi->getUserLocationByUID: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetUserLocationResponseValue***](SWGGetUserLocationResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setUserLocation**
```objc
-(NSNumber*) setUserLocationWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSetUserLocationParamBody*) body
        completionHandler: (void (^)(SWGSetUserLocationResponseValue* output, NSError* error)) handler;
```

User set location

User set location

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGSetUserLocationParamBody* body = [[SWGSetUserLocationParamBody alloc] init]; //  (optional)

SWGLocationApi*apiInstance = [[SWGLocationApi alloc] init];

// User set location
[apiInstance setUserLocationWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGSetUserLocationResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGLocationApi->setUserLocation: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGSetUserLocationParamBody***](SWGSetUserLocationParamBody*.md)|  | [optional] 

### Return type

[**SWGSetUserLocationResponseValue***](SWGSetUserLocationResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

