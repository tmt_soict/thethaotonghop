# SWGSetUserLocationParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **NSNumber*** | latitude | [optional] 
**lng** | **NSNumber*** | longitude | [optional] 
**address** | **NSString*** |  | [optional] 
**time** | **NSNumber*** | epoch time | [optional] 
**tag** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


