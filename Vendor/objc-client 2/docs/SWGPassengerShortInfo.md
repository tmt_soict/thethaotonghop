# SWGPassengerShortInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**passengerName** | **NSString*** | Name of passenger has request | 
**passengerPhone** | **NSString*** | Phone number of passenger has request | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


