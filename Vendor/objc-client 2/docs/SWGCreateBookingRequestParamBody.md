# SWGCreateBookingRequestParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**pickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**dropoffTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**vehicleQuality** | **NSNumber*** | car&#39;s quality ECO &#x3D; 1 | 6 &#x3D; HIGH &amp; LUXURY | [optional] 
**vehicleType** | **NSNumber*** | car&#39;s capacity 4, 7, 9, 16 .. | [optional] 
**load** | **NSNumber*** | number of passengers | [optional] 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | [optional] 
**status** | **NSNumber*** | booking status | 0 &#x3D; PENDING | 1 &#x3D; PREPROCESSED | 2 &#x3D; PROCESSED | 10 &#x3D; RUNNING | 15 &#x3D; COMPLETED | 16 &#x3D; CANCELED | [optional] 
**distance** | **NSNumber*** |  | [optional] 
**priceRange** | [**SWGCostRange***](SWGCostRange.md) |  | [optional] 
**operatorId** | **NSNumber*** |  | [optional] [default to @0]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


