# SWGGetListPassengerOfTripv202ResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**NSArray&lt;SWGTicketOfTripv202&gt;***](SWGTicketOfTripv202.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


