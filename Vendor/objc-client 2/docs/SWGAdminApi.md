# SWGAdminApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBookingRequestByAdmin**](SWGAdminApi.md#createbookingrequestbyadmin) | **POST** /admin/bookings | Admin creates a booking through the request of consumer&#39;s phone call
[**createNewAdmin**](SWGAdminApi.md#createnewadmin) | **POST** /admin/employees/admins | Create new admin employee
[**createNewDriver**](SWGAdminApi.md#createnewdriver) | **POST** /admin/employees/drivers | Create new driver employee
[**createNewDriverv201**](SWGAdminApi.md#createnewdriverv201) | **POST** /admin/employees/drivers/v201 | Create new driver employee v201
[**createNewTrip**](SWGAdminApi.md#createnewtrip) | **POST** /admin/trips | create new trip
[**deleteAdmin**](SWGAdminApi.md#deleteadmin) | **DELETE** /admin/employees/admin/{admin_id} | Delete an admin
[**deleteDriver**](SWGAdminApi.md#deletedriver) | **DELETE** /admin/employees/drivers/{driver_id} | Delete a driver
[**getAdmin**](SWGAdminApi.md#getadmin) | **GET** /admin/employees/admin/{admin_id} | Get admin info by id
[**getAllAdmins**](SWGAdminApi.md#getalladmins) | **GET** /admin/employees/admins | Get all admins
[**getAllBookingsOfDate**](SWGAdminApi.md#getallbookingsofdate) | **GET** /admin/bookings/{selected_date} | Get all bookings of  {selected_date}
[**getAllDrivers**](SWGAdminApi.md#getalldrivers) | **GET** /admin/employees/drivers | Get all drivers
[**getAllDriversv201**](SWGAdminApi.md#getalldriversv201) | **GET** /admin/employees/drivers/v201 | Get all drivers v201
[**getAllOperators**](SWGAdminApi.md#getalloperators) | **GET** /admin/operators | Get all operators
[**getAllRequestsOfDatev201**](SWGAdminApi.md#getallrequestsofdatev201) | **GET** /admin/requests/{selected_date}/v2_0_1 | Get all requests of {selected_date} v2.0.1
[**getAllTripsOfDate**](SWGAdminApi.md#getalltripsofdate) | **GET** /admin/trips/{selected_date} | Get all trips of  {selected_date}
[**getAuthenticatedAdmin**](SWGAdminApi.md#getauthenticatedadmin) | **GET** /admin | Get the authenticated admin
[**getDriver**](SWGAdminApi.md#getdriver) | **GET** /admin/employees/drivers/{driver_id} | Get driver info by id
[**getEmployees**](SWGAdminApi.md#getemployees) | **GET** /admin/employees | Get all employees
[**getFreeDrivers**](SWGAdminApi.md#getfreedrivers) | **GET** /admin/employees/drivers/free/{selected_date} | Get all free drivers in {selected_date}
[**getLastNBookings**](SWGAdminApi.md#getlastnbookings) | **GET** /admin/bookings/last/{number_bookings} | Get last {number_bookings} bookings
[**getLastNTrips**](SWGAdminApi.md#getlastntrips) | **GET** /admin/trips/last/{number_trips} | Get last {number_trips} trips
[**getPageTripsByAdmin**](SWGAdminApi.md#getpagetripsbyadmin) | **GET** /admin/trips | Get page trips of authenticated admin by number of day(s)
[**getPassengerByPhone**](SWGAdminApi.md#getpassengerbyphone) | **GET** /admin/passenger/{phone} | Get name of passenger by phone
[**getPassengerByPhonev201**](SWGAdminApi.md#getpassengerbyphonev201) | **GET** /admin/passenger/{phone}/v2_0_1 | Get name of passenger by phone v201
[**getSchedulesOfDriver**](SWGAdminApi.md#getschedulesofdriver) | **GET** /admin/employees/driver/{driver_id}/schedules | Get schedules from one day to the end of month of one driver
[**getUpdateHistoryOfTrip**](SWGAdminApi.md#getupdatehistoryoftrip) | **GET** /admin/trips/{trip_id}/update_history | Get update history of trip by id
[**getUpdateHistoryOfTripByTime**](SWGAdminApi.md#getupdatehistoryoftripbytime) | **GET** /admin/trips/{trip_id}/update_history/{time} | Get update history of trip by id and time
[**searchDriverByPhone**](SWGAdminApi.md#searchdriverbyphone) | **GET** /admin/employees/drivers/seach/{phone} | Search driver by phone
[**searchDriverByPhonev201**](SWGAdminApi.md#searchdriverbyphonev201) | **GET** /admin/employees/drivers/seach/{phone}/v201 | Search driver by phone v201
[**sendHiringInvitation**](SWGAdminApi.md#sendhiringinvitation) | **POST** /admin/invitations | send hiring invitation
[**setScheduleOfDriver**](SWGAdminApi.md#setscheduleofdriver) | **POST** /admin/employees/driver/{driver_id}/schedules | Set schedule of day for driver
[**setScheduleOfDriverv201**](SWGAdminApi.md#setscheduleofdriverv201) | **POST** /admin/employees/driver/{driver_id}/schedules/v2_0_1 | Set schedule of day(s) for driver
[**updateAdmin**](SWGAdminApi.md#updateadmin) | **PUT** /admin/employees/admin/{admin_id} | Update info of an admin
[**updateAuthenticatedAdmin**](SWGAdminApi.md#updateauthenticatedadmin) | **PUT** /admin | Update the authenticated admin
[**updateDriver**](SWGAdminApi.md#updatedriver) | **PUT** /admin/employees/drivers/{driver_id} | Update info of a driver
[**updateOperatorInfo**](SWGAdminApi.md#updateoperatorinfo) | **PUT** /admin/operator | Update operator of the authenticated admin


# **createBookingRequestByAdmin**
```objc
-(NSNumber*) createBookingRequestByAdminWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGCreateBookingRequestByAdminParamBody*) body
        completionHandler: (void (^)(SWGCreateBookingRequestResponseValue* output, NSError* error)) handler;
```

Admin creates a booking through the request of consumer's phone call

Admin creates a booking through the request of consumer's phone call

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGCreateBookingRequestByAdminParamBody* body = [[SWGCreateBookingRequestByAdminParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Admin creates a booking through the request of consumer's phone call
[apiInstance createBookingRequestByAdminWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGCreateBookingRequestResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->createBookingRequestByAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGCreateBookingRequestByAdminParamBody***](SWGCreateBookingRequestByAdminParamBody*.md)|  | [optional] 

### Return type

[**SWGCreateBookingRequestResponseValue***](SWGCreateBookingRequestResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createNewAdmin**
```objc
-(NSNumber*) createNewAdminWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGCreateNewAdminParamBody*) body
        completionHandler: (void (^)(SWGCreateNewAdminResponseValue* output, NSError* error)) handler;
```

Create new admin employee

Create new admin employee.

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGCreateNewAdminParamBody* body = [[SWGCreateNewAdminParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Create new admin employee
[apiInstance createNewAdminWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGCreateNewAdminResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->createNewAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGCreateNewAdminParamBody***](SWGCreateNewAdminParamBody*.md)|  | [optional] 

### Return type

[**SWGCreateNewAdminResponseValue***](SWGCreateNewAdminResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createNewDriver**
```objc
-(NSNumber*) createNewDriverWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGCreateNewDriverParamBody*) body
        completionHandler: (void (^)(SWGCreateNewDriverResponseValue* output, NSError* error)) handler;
```

Create new driver employee

Create new driver employee

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGCreateNewDriverParamBody* body = [[SWGCreateNewDriverParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Create new driver employee
[apiInstance createNewDriverWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGCreateNewDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->createNewDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGCreateNewDriverParamBody***](SWGCreateNewDriverParamBody*.md)|  | [optional] 

### Return type

[**SWGCreateNewDriverResponseValue***](SWGCreateNewDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createNewDriverv201**
```objc
-(NSNumber*) createNewDriverv201WithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGCreateNewDriverParamBody*) body
        completionHandler: (void (^)(SWGCreateNewDriverResponseValue* output, NSError* error)) handler;
```

Create new driver employee v201

Create new driver employee v201

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGCreateNewDriverParamBody* body = [[SWGCreateNewDriverParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Create new driver employee v201
[apiInstance createNewDriverv201WithAver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGCreateNewDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->createNewDriverv201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGCreateNewDriverParamBody***](SWGCreateNewDriverParamBody*.md)|  | [optional] 

### Return type

[**SWGCreateNewDriverResponseValue***](SWGCreateNewDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createNewTrip**
```objc
-(NSNumber*) createNewTripWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGCreateNewTripParamBody*) body
        completionHandler: (void (^)(SWGCreateNewTripResponseValue* output, NSError* error)) handler;
```

create new trip

create new trip

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGCreateNewTripParamBody* body = [[SWGCreateNewTripParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// create new trip
[apiInstance createNewTripWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGCreateNewTripResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->createNewTrip: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGCreateNewTripParamBody***](SWGCreateNewTripParamBody*.md)|  | [optional] 

### Return type

[**SWGCreateNewTripResponseValue***](SWGCreateNewTripResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteAdmin**
```objc
-(NSNumber*) deleteAdminWithAdminId: (NSNumber*) adminId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGDeleteAdminResponseValue* output, NSError* error)) handler;
```

Delete an admin

Delete an Admin

### Example 
```objc

NSNumber* adminId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Delete an admin
[apiInstance deleteAdminWithAdminId:adminId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGDeleteAdminResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->deleteAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adminId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGDeleteAdminResponseValue***](SWGDeleteAdminResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteDriver**
```objc
-(NSNumber*) deleteDriverWithDriverId: (NSNumber*) driverId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGDeleteDriverResponseValue* output, NSError* error)) handler;
```

Delete a driver

Delete an driver

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Delete a driver
[apiInstance deleteDriverWithDriverId:driverId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGDeleteDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->deleteDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGDeleteDriverResponseValue***](SWGDeleteDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAdmin**
```objc
-(NSNumber*) getAdminWithAdminId: (NSNumber*) adminId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAdminResponseValue* output, NSError* error)) handler;
```

Get admin info by id

Get admin info by id

### Example 
```objc

NSNumber* adminId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get admin info by id
[apiInstance getAdminWithAdminId:adminId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAdminResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adminId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAdminResponseValue***](SWGGetAdminResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllAdmins**
```objc
-(NSNumber*) getAllAdminsWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllAdminsResponseValue* output, NSError* error)) handler;
```

Get all admins

Get all admins

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all admins
[apiInstance getAllAdminsWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllAdminsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllAdmins: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllAdminsResponseValue***](SWGGetAllAdminsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllBookingsOfDate**
```objc
-(NSNumber*) getAllBookingsOfDateWithSelectedDate: (NSNumber*) selectedDate
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllBookingsOfDateResponseValue* output, NSError* error)) handler;
```

Get all bookings of  {selected_date}

Get all bookings of  {selected_date}

### Example 
```objc

NSNumber* selectedDate = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all bookings of  {selected_date}
[apiInstance getAllBookingsOfDateWithSelectedDate:selectedDate
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllBookingsOfDateResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllBookingsOfDate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selectedDate** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllBookingsOfDateResponseValue***](SWGGetAllBookingsOfDateResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllDrivers**
```objc
-(NSNumber*) getAllDriversWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllDriversResponseValue* output, NSError* error)) handler;
```

Get all drivers

Get all drivers

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all drivers
[apiInstance getAllDriversWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllDriversResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllDrivers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllDriversResponseValue***](SWGGetAllDriversResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllDriversv201**
```objc
-(NSNumber*) getAllDriversv201WithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllDriversResponseValue* output, NSError* error)) handler;
```

Get all drivers v201

Get all drivers v201

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all drivers v201
[apiInstance getAllDriversv201WithAver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllDriversResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllDriversv201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllDriversResponseValue***](SWGGetAllDriversResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllOperators**
```objc
-(NSNumber*) getAllOperatorsWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetAllOperatorsResponseValue* output, NSError* error)) handler;
```

Get all operators

Get all operators

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all operators
[apiInstance getAllOperatorsWithAver:aver
              ctype:ctype
          completionHandler: ^(SWGGetAllOperatorsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllOperators: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetAllOperatorsResponseValue***](SWGGetAllOperatorsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllRequestsOfDatev201**
```objc
-(NSNumber*) getAllRequestsOfDatev201WithSelectedDate: (NSNumber*) selectedDate
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllRequestsOfDatev201ResponseValue* output, NSError* error)) handler;
```

Get all requests of {selected_date} v2.0.1

Get all requests of {selected_date} v2.0.1

### Example 
```objc

NSNumber* selectedDate = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all requests of {selected_date} v2.0.1
[apiInstance getAllRequestsOfDatev201WithSelectedDate:selectedDate
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllRequestsOfDatev201ResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllRequestsOfDatev201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selectedDate** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllRequestsOfDatev201ResponseValue***](SWGGetAllRequestsOfDatev201ResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllTripsOfDate**
```objc
-(NSNumber*) getAllTripsOfDateWithSelectedDate: (NSNumber*) selectedDate
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllTripsOfDateResponseValue* output, NSError* error)) handler;
```

Get all trips of  {selected_date}

Get all trips of  {selected_date}

### Example 
```objc

NSNumber* selectedDate = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all trips of  {selected_date}
[apiInstance getAllTripsOfDateWithSelectedDate:selectedDate
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllTripsOfDateResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAllTripsOfDate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selectedDate** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllTripsOfDateResponseValue***](SWGGetAllTripsOfDateResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAuthenticatedAdmin**
```objc
-(NSNumber*) getAuthenticatedAdminWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetAuthenticatedAdminReponseValue* output, NSError* error)) handler;
```

Get the authenticated admin

Get info of the authenticated admin

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get the authenticated admin
[apiInstance getAuthenticatedAdminWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetAuthenticatedAdminReponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getAuthenticatedAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetAuthenticatedAdminReponseValue***](SWGGetAuthenticatedAdminReponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getDriver**
```objc
-(NSNumber*) getDriverWithDriverId: (NSNumber*) driverId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetDriverResponseValue* output, NSError* error)) handler;
```

Get driver info by id

Get driver info by id

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get driver info by id
[apiInstance getDriverWithDriverId:driverId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetDriverResponseValue***](SWGGetDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getEmployees**
```objc
-(NSNumber*) getEmployeesWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetEmployeeResponseValue* output, NSError* error)) handler;
```

Get all employees

Get all employees

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all employees
[apiInstance getEmployeesWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetEmployeeResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getEmployees: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetEmployeeResponseValue***](SWGGetEmployeeResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFreeDrivers**
```objc
-(NSNumber*) getFreeDriversWithSelectedDate: (NSNumber*) selectedDate
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetAllFreeDriversResponseValue* output, NSError* error)) handler;
```

Get all free drivers in {selected_date}

Get all free drivers in {selected_date}

### Example 
```objc

NSNumber* selectedDate = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get all free drivers in {selected_date}
[apiInstance getFreeDriversWithSelectedDate:selectedDate
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetAllFreeDriversResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getFreeDrivers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **selectedDate** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetAllFreeDriversResponseValue***](SWGGetAllFreeDriversResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getLastNBookings**
```objc
-(NSNumber*) getLastNBookingsWithNumberBookings: (NSNumber*) numberBookings
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetLastNBookingsResponseValue* output, NSError* error)) handler;
```

Get last {number_bookings} bookings

Get last {number_bookings} bookings

### Example 
```objc

NSNumber* numberBookings = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get last {number_bookings} bookings
[apiInstance getLastNBookingsWithNumberBookings:numberBookings
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetLastNBookingsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getLastNBookings: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numberBookings** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetLastNBookingsResponseValue***](SWGGetLastNBookingsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getLastNTrips**
```objc
-(NSNumber*) getLastNTripsWithNumberTrips: (NSNumber*) numberTrips
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetLastNTripsResponseValue* output, NSError* error)) handler;
```

Get last {number_trips} trips

Get last {number_trips} trips

### Example 
```objc

NSNumber* numberTrips = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get last {number_trips} trips
[apiInstance getLastNTripsWithNumberTrips:numberTrips
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetLastNTripsResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getLastNTrips: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numberTrips** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetLastNTripsResponseValue***](SWGGetLastNTripsResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPageTripsByAdmin**
```objc
-(NSNumber*) getPageTripsByAdminWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastday: (NSNumber*) lastday
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
        completionHandler: (void (^)(SWGGetPageTripResponseValue* output, NSError* error)) handler;
```

Get page trips of authenticated admin by number of day(s)

Get page trips of authenticated admin by number of day(s)

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* lastday = @789; // id of last trip (optional)
NSNumber* minsize = @56; // the number of dates you want to make request (optional)
NSNumber* ordertype = @56; // 0 = old -> new | 1 = new -> old (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get page trips of authenticated admin by number of day(s)
[apiInstance getPageTripsByAdminWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              lastday:lastday
              minsize:minsize
              ordertype:ordertype
          completionHandler: ^(SWGGetPageTripResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getPageTripsByAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **lastday** | **NSNumber***| id of last trip | [optional] 
 **minsize** | **NSNumber***| the number of dates you want to make request | [optional] 
 **ordertype** | **NSNumber***| 0 &#x3D; old -&gt; new | 1 &#x3D; new -&gt; old | [optional] 

### Return type

[**SWGGetPageTripResponseValue***](SWGGetPageTripResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPassengerByPhone**
```objc
-(NSNumber*) getPassengerByPhoneWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetPassengerByPhoneResponseValue* output, NSError* error)) handler;
```

Get name of passenger by phone

Get name of passenger by phone

### Example 
```objc

NSString* phone = @"phone_example"; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get name of passenger by phone
[apiInstance getPassengerByPhoneWithPhone:phone
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetPassengerByPhoneResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getPassengerByPhone: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetPassengerByPhoneResponseValue***](SWGGetPassengerByPhoneResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPassengerByPhonev201**
```objc
-(NSNumber*) getPassengerByPhonev201WithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetPassengerByPhoneResponseValue* output, NSError* error)) handler;
```

Get name of passenger by phone v201

Get name of passenger by phone v201

### Example 
```objc

NSString* phone = @"phone_example"; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get name of passenger by phone v201
[apiInstance getPassengerByPhonev201WithPhone:phone
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetPassengerByPhoneResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getPassengerByPhonev201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetPassengerByPhoneResponseValue***](SWGGetPassengerByPhoneResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getSchedulesOfDriver**
```objc
-(NSNumber*) getSchedulesOfDriverWithDriverId: (NSNumber*) driverId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    fromdate: (NSNumber*) fromdate
        completionHandler: (void (^)(SWGGetScheduleResponseValue* output, NSError* error)) handler;
```

Get schedules from one day to the end of month of one driver

Get schedules from one day to the end of month of one driver

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSNumber* fromdate = @789; // epoch time, default value is current time (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get schedules from one day to the end of month of one driver
[apiInstance getSchedulesOfDriverWithDriverId:driverId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
              fromdate:fromdate
          completionHandler: ^(SWGGetScheduleResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getSchedulesOfDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **fromdate** | **NSNumber***| epoch time, default value is current time | [optional] 

### Return type

[**SWGGetScheduleResponseValue***](SWGGetScheduleResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getUpdateHistoryOfTrip**
```objc
-(NSNumber*) getUpdateHistoryOfTripWithTripId: (NSNumber*) tripId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetTripRevisionResponseValue* output, NSError* error)) handler;
```

Get update history of trip by id

Get update history of trip by id

### Example 
```objc

NSNumber* tripId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get update history of trip by id
[apiInstance getUpdateHistoryOfTripWithTripId:tripId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetTripRevisionResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getUpdateHistoryOfTrip: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetTripRevisionResponseValue***](SWGGetTripRevisionResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getUpdateHistoryOfTripByTime**
```objc
-(NSNumber*) getUpdateHistoryOfTripByTimeWithTripId: (NSNumber*) tripId
    time: (NSNumber*) time
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetTripRevisionByTimeResponseValue* output, NSError* error)) handler;
```

Get update history of trip by id and time

Get update history of trip by id and time

### Example 
```objc

NSNumber* tripId = @789; // 
NSNumber* time = @789; // (epochtime) when the trip is modified
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Get update history of trip by id and time
[apiInstance getUpdateHistoryOfTripByTimeWithTripId:tripId
              time:time
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetTripRevisionByTimeResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->getUpdateHistoryOfTripByTime: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tripId** | **NSNumber***|  | 
 **time** | **NSNumber***| (epochtime) when the trip is modified | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetTripRevisionByTimeResponseValue***](SWGGetTripRevisionByTimeResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **searchDriverByPhone**
```objc
-(NSNumber*) searchDriverByPhoneWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetDriverResponseValue* output, NSError* error)) handler;
```

Search driver by phone

Search driver by phone

### Example 
```objc

NSString* phone = @"phone_example"; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Search driver by phone
[apiInstance searchDriverByPhoneWithPhone:phone
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->searchDriverByPhone: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetDriverResponseValue***](SWGGetDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **searchDriverByPhonev201**
```objc
-(NSNumber*) searchDriverByPhonev201WithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
        completionHandler: (void (^)(SWGGetDriverResponseValue* output, NSError* error)) handler;
```

Search driver by phone v201

Search driver by phone v201

### Example 
```objc

NSString* phone = @"phone_example"; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Search driver by phone v201
[apiInstance searchDriverByPhonev201WithPhone:phone
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
          completionHandler: ^(SWGGetDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->searchDriverByPhonev201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 

### Return type

[**SWGGetDriverResponseValue***](SWGGetDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sendHiringInvitation**
```objc
-(NSNumber*) sendHiringInvitationWithAver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGSendHiringInvitationParamBody*) body
        completionHandler: (void (^)(SWGSendHiringInvitationResponseValue* output, NSError* error)) handler;
```

send hiring invitation

send hiring invitation

### Example 
```objc

NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGSendHiringInvitationParamBody* body = [[SWGSendHiringInvitationParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// send hiring invitation
[apiInstance sendHiringInvitationWithAver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGSendHiringInvitationResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->sendHiringInvitation: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGSendHiringInvitationParamBody***](SWGSendHiringInvitationParamBody*.md)|  | [optional] 

### Return type

[**SWGSendHiringInvitationResponseValue***](SWGSendHiringInvitationResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setScheduleOfDriver**
```objc
-(NSNumber*) setScheduleOfDriverWithDriverId: (NSNumber*) driverId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSchedule*) body
        completionHandler: (void (^)(SWGSetScheduleResponseValue* output, NSError* error)) handler;
```

Set schedule of day for driver

Set schedule of day for driver

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGSchedule* body = [[SWGSchedule alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Set schedule of day for driver
[apiInstance setScheduleOfDriverWithDriverId:driverId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGSetScheduleResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->setScheduleOfDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGSchedule***](SWGSchedule*.md)|  | [optional] 

### Return type

[**SWGSetScheduleResponseValue***](SWGSetScheduleResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setScheduleOfDriverv201**
```objc
-(NSNumber*) setScheduleOfDriverv201WithDriverId: (NSNumber*) driverId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSchedules*) body
        completionHandler: (void (^)(SWGSetSchedulesResponseValue* output, NSError* error)) handler;
```

Set schedule of day(s) for driver

Set schedule of day(s) for driver

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGSchedules* body = [[SWGSchedules alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Set schedule of day(s) for driver
[apiInstance setScheduleOfDriverv201WithDriverId:driverId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGSetSchedulesResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->setScheduleOfDriverv201: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGSchedules***](SWGSchedules*.md)|  | [optional] 

### Return type

[**SWGSetSchedulesResponseValue***](SWGSetSchedulesResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateAdmin**
```objc
-(NSNumber*) updateAdminWithAdminId: (NSNumber*) adminId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGUpdateAdminParamBody*) body
        completionHandler: (void (^)(SWGUpdateAdminResponseValue* output, NSError* error)) handler;
```

Update info of an admin

Update info of an admin

### Example 
```objc

NSNumber* adminId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGUpdateAdminParamBody* body = [[SWGUpdateAdminParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Update info of an admin
[apiInstance updateAdminWithAdminId:adminId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGUpdateAdminResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->updateAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **adminId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGUpdateAdminParamBody***](SWGUpdateAdminParamBody*.md)|  | [optional] 

### Return type

[**SWGUpdateAdminResponseValue***](SWGUpdateAdminResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateAuthenticatedAdmin**
```objc
-(NSNumber*) updateAuthenticatedAdminWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGUpdateAuthenticatedAdminParamBody*) body
        completionHandler: (void (^)(SWGUpdateAuthenticatedAdminReponseValue* output, NSError* error)) handler;
```

Update the authenticated admin

Update info of the authenticated admin

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGUpdateAuthenticatedAdminParamBody* body = [[SWGUpdateAuthenticatedAdminParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Update the authenticated admin
[apiInstance updateAuthenticatedAdminWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGUpdateAuthenticatedAdminReponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->updateAuthenticatedAdmin: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGUpdateAuthenticatedAdminParamBody***](SWGUpdateAuthenticatedAdminParamBody*.md)|  | [optional] 

### Return type

[**SWGUpdateAuthenticatedAdminReponseValue***](SWGUpdateAuthenticatedAdminReponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateDriver**
```objc
-(NSNumber*) updateDriverWithDriverId: (NSNumber*) driverId
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    sessionkey: (NSString*) sessionkey
    body: (SWGUpdateDriverParamBody*) body
        completionHandler: (void (^)(SWGUpdateDriverResponseValue* output, NSError* error)) handler;
```

Update info of a driver

Update info of a driver

### Example 
```objc

NSNumber* driverId = @789; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
NSString* sessionkey = @"sessionkey_example"; //  (optional)
SWGUpdateDriverParamBody* body = [[SWGUpdateDriverParamBody alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Update info of a driver
[apiInstance updateDriverWithDriverId:driverId
              aver:aver
              ctype:ctype
              sessionkey:sessionkey
              body:body
          completionHandler: ^(SWGUpdateDriverResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->updateDriver: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **driverId** | **NSNumber***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **sessionkey** | **NSString***|  | [optional] 
 **body** | [**SWGUpdateDriverParamBody***](SWGUpdateDriverParamBody*.md)|  | [optional] 

### Return type

[**SWGUpdateDriverResponseValue***](SWGUpdateDriverResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateOperatorInfo**
```objc
-(NSNumber*) updateOperatorInfoWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGOperator*) body
        completionHandler: (void (^)(SWGUpdateOperatorInfoResponseValue* output, NSError* error)) handler;
```

Update operator of the authenticated admin

Update operator info of the authenticated admin

### Example 
```objc

NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGOperator* body = [[SWGOperator alloc] init]; //  (optional)

SWGAdminApi*apiInstance = [[SWGAdminApi alloc] init];

// Update operator of the authenticated admin
[apiInstance updateOperatorInfoWithSessionkey:sessionkey
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGUpdateOperatorInfoResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGAdminApi->updateOperatorInfo: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGOperator***](SWGOperator*.md)|  | [optional] 

### Return type

[**SWGUpdateOperatorInfoResponseValue***](SWGUpdateOperatorInfoResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

