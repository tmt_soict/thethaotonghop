#import "SWGCreateBookingRequestParamBody.h"

@implementation SWGCreateBookingRequestParamBody

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"from": @"from", @"to": @"to", @"pickup_time": @"pickupTime", @"dropoff_time": @"dropoffTime", @"load": @"load", @"is_sharing": @"isSharing", @"status": @"status", @"distance": @"distance", @"price_range": @"priceRange" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"from", @"to", @"pickupTime", @"dropoffTime", @"load", @"isSharing", @"status", @"distance", @"priceRange"];
  return [optionalProperties containsObject:propertyName];
}

@end
