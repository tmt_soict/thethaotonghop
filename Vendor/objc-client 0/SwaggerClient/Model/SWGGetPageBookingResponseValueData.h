#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* 123 XE API - v2
* Tài liệu mô tả 123 xe api - http://dev.api.123xe.vn/v2
*
* OpenAPI spec version: 2.0.0
* Contact: hieubq@vng.com.vn
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#import "SWGBooking.h"
#import "SWGGetBookingMeta.h"


@protocol SWGGetPageBookingResponseValueData
@end

@interface SWGGetPageBookingResponseValueData : SWGObject


@property(nonatomic) SWGGetBookingMeta* meta;

@property(nonatomic) NSArray<SWGBooking>* bookings;

@end
