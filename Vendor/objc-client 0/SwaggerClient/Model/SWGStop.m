#import "SWGStop.h"

@implementation SWGStop

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"request_id": @"requestId", @"location": @"location", @"required_pickup_time": @"requiredPickupTime", @"arrive_time": @"arriveTime", @"stop_type": @"stopType", @"passengers": @"passengers", @"number_passenger": @"numberPassenger", @"is_shared_request": @"isSharedRequest" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[];
  return [optionalProperties containsObject:propertyName];
}

@end
