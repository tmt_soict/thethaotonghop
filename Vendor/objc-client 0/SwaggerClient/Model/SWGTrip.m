#import "SWGTrip.h"

@implementation SWGTrip

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"driver_id": @"driverId", @"start_time": @"startTime", @"from": @"from", @"to": @"to", @"total_passengers": @"totalPassengers", @"status": @"status", @"income": @"income" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", ];
  return [optionalProperties containsObject:propertyName];
}

@end
