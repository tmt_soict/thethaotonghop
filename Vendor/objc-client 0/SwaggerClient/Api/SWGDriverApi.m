#import "SWGDriverApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGGetAuthenticatedDriverResponseValue.h"
#import "SWGGetNotificationsResponseValue.h"
#import "SWGGetPageTripResponseValue.h"
#import "SWGGetScheduleResponseValue.h"
#import "SWGGetTripHistoryResponseValue.h"
#import "SWGGetTripByDateResponseValue.h"
#import "SWGProcessNotifyResponseValue.h"
#import "SWGProcessNotifyParamBody.h"
#import "SWGSetScheduleResponseValue.h"
#import "SWGSchedule.h"
#import "SWGSchedules.h"
#import "SWGSetSchedulesResponseValue.h"
#import "SWGUpdateAuthenticatedDriverParamBody.h"
#import "SWGUpdateAuthenticatedDriverResponseValue.h"


@interface SWGDriverApi ()

@property (nonatomic, strong) NSMutableDictionary *defaultHeaders;

@end

@implementation SWGDriverApi

NSString* kSWGDriverApiErrorDomain = @"SWGDriverApiErrorDomain";
NSInteger kSWGDriverApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    self = [super init];
    if (self) {
        SWGConfiguration *config = [SWGConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[SWGApiClient alloc] init];
        }
        _apiClient = config.apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+ (instancetype)sharedAPI {
    static SWGDriverApi *sharedAPI;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.defaultHeaders[key];
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self setDefaultHeaderValue:value forKey:key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(NSUInteger) requestQueueSize {
    return [SWGApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Get the authenticated driver
/// Get the authenticated driver
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGGetAuthenticatedDriverResponseValue*
///
-(NSNumber*) getAuthenticatedDriverWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGGetAuthenticatedDriverResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetAuthenticatedDriverResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetAuthenticatedDriverResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get notifications
/// Get notifications
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGGetNotificationsResponseValue*
///
-(NSNumber*) getDriverNotificationsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGGetNotificationsResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/notifications"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetNotificationsResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetNotificationsResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get page trips of authenticated driver
/// Get page trips of authenticated driver
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param lastid id of last item / last date (optional)
///
///  @param minsize minimum items / request (optional)
///
///  @param ordertype 0 = old -> new | 1 = new -> old (optional)
///
///  @returns SWGGetPageTripResponseValue*
///
-(NSNumber*) getPageTripsWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastid: (NSNumber*) lastid
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
    completionHandler: (void (^)(SWGGetPageTripResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/trips"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    if (lastid != nil) {
        headerParams[@"Lastid"] = lastid;
    }
    if (minsize != nil) {
        headerParams[@"Minsize"] = minsize;
    }
    if (ordertype != nil) {
        headerParams[@"Ordertype"] = ordertype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetPageTripResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetPageTripResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get schedules from one day to the end of month
/// Get schedules from one day to the end of month
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param fromdate epoch time, default value is current time (optional)
///
///  @param todate epoch time, default value is current time (optional)
///
///  @returns SWGGetScheduleResponseValue*
///
-(NSNumber*) getSchedulesWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    fromdate: (NSNumber*) fromdate
    todate: (NSNumber*) todate
    completionHandler: (void (^)(SWGGetScheduleResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/schedules"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    if (fromdate != nil) {
        headerParams[@"Fromdate"] = fromdate;
    }
    if (todate != nil) {
        headerParams[@"Todate"] = todate;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetScheduleResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetScheduleResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get page trips of authenticated driver
/// Get page trips of authenticated driver
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param lastid id of last item / last date (optional)
///
///  @param minsize minimum items / request (optional)
///
///  @param ordertype 0 = old -> new | 1 = new -> old (optional)
///
///  @returns SWGGetTripHistoryResponseValue*
///
-(NSNumber*) getTripHistoryWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    lastid: (NSNumber*) lastid
    minsize: (NSNumber*) minsize
    ordertype: (NSNumber*) ordertype
    completionHandler: (void (^)(SWGGetTripHistoryResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/trip_history"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    if (lastid != nil) {
        headerParams[@"Lastid"] = lastid;
    }
    if (minsize != nil) {
        headerParams[@"Minsize"] = minsize;
    }
    if (ordertype != nil) {
        headerParams[@"Ordertype"] = ordertype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetTripHistoryResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetTripHistoryResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get trips by date of authenticated driver
/// Get trips by date of authenticated driver
///  @param selectedDate  
///
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGGetTripByDateResponseValue*
///
-(NSNumber*) getTripsByDateWithSelectedDate: (NSNumber*) selectedDate
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGGetTripByDateResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'selectedDate' is set
    if (selectedDate == nil) {
        NSParameterAssert(selectedDate);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"selectedDate"] };
            NSError* error = [NSError errorWithDomain:kSWGDriverApiErrorDomain code:kSWGDriverApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/trips/{selected_date}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (selectedDate != nil) {
        pathParams[@"selected_date"] = selectedDate;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetTripByDateResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetTripByDateResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get page trips of authenticated driver
/// Get page trips of authenticated driver
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGProcessNotifyResponseValue*
///
-(NSNumber*) processNotifyWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGProcessNotifyParamBody*) body
    completionHandler: (void (^)(SWGProcessNotifyResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/notifications"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGProcessNotifyResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGProcessNotifyResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Set schedule of one day
/// Set schedule of one day
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGSetScheduleResponseValue*
///
-(NSNumber*) setScheduleWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSchedule*) body
    completionHandler: (void (^)(SWGSetScheduleResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/schedules"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGSetScheduleResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGSetScheduleResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Set schedule of day(s)
/// Set schedule of day(s)
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGSetSchedulesResponseValue*
///
-(NSNumber*) setSchedulev201WithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSchedules*) body
    completionHandler: (void (^)(SWGSetSchedulesResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver/schedules/v2_0_1"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGSetSchedulesResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGSetSchedulesResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Update authenticated driver
/// Update authenticated driver
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGUpdateAuthenticatedDriverResponseValue*
///
-(NSNumber*) updateAuthenticatedDriverWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGUpdateAuthenticatedDriverParamBody*) body
    completionHandler: (void (^)(SWGUpdateAuthenticatedDriverResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/driver"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"PUT"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGUpdateAuthenticatedDriverResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGUpdateAuthenticatedDriverResponseValue*)data, error);
                                }
                           }
          ];
}



@end
