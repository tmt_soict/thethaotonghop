# SWGBooking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingId** | **NSNumber*** |  | [optional] 
**requestId** | **NSNumber*** |  | [optional] 
**passengerId** | **NSNumber*** |  | [optional] 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**pickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**estimatePickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**dropoffTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**load** | **NSNumber*** | number of passengers | 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | 
**status** | **NSNumber*** | booking status | 0 &#x3D; PENDING | 1 &#x3D; PREPROCESSED | 2 &#x3D; PROCESSED | 10 &#x3D; RUNNING | 15 &#x3D; COMPLETED | 16 &#x3D; CANCELED | 
**distance** | **NSNumber*** |  | 
**priceRange** | [**SWGCostRange***](SWGCostRange.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


