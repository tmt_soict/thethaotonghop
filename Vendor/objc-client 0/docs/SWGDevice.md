# SWGDevice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**osType** | **NSNumber*** | device type | 0 &#x3D; Android | 1 &#x3D; IOS | 2 &#x3D; Winphone | 
**deviceToken** | **NSString*** |  | 
**clientType** | **NSNumber*** | client type | 0 &#x3D; passenger | 1 &#x3D; partner | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


