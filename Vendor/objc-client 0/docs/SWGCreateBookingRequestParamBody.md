# SWGCreateBookingRequestParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**pickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**dropoffTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**load** | **NSNumber*** | number of passengers | [optional] 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | [optional] 
**status** | **NSNumber*** | booking status | 0 &#x3D; PENDING | 1 &#x3D; PREPROCESSED | 2 &#x3D; PROCESSED | 10 &#x3D; RUNNING | 15 &#x3D; COMPLETED | 16 &#x3D; CANCELED | [optional] 
**distance** | **NSNumber*** |  | [optional] 
**priceRange** | [**SWGCostRange***](SWGCostRange.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


