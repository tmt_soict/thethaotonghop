# SWGTripInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driverId** | **NSNumber*** |  | [optional] 
**startTime** | **NSNumber*** |  | [optional] 
**distance** | **NSNumber*** |  | [optional] 
**status** | **NSNumber*** | WAITING &#x3D; 0 | RUNNING &#x3D; 10 | FINISHED &#x3D; 15 | CANCELED &#x3D; 16 | DELETED &#x3D; 40 | [optional] 
**totalPassengers** | **NSNumber*** |  | [optional] 
**income** | **NSNumber*** | total price of trip | [optional] 
**route** | [**NSArray&lt;SWGStopInfo&gt;***](SWGStopInfo.md) | list stop_info | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


