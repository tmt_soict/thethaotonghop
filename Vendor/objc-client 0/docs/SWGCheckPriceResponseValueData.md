# SWGCheckPriceResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance** | **NSNumber*** |  | [optional] 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**sharingCost** | [**SWGCostRange***](SWGCostRange.md) |  | [optional] 
**privateCost** | [**SWGCostRange***](SWGCostRange.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


