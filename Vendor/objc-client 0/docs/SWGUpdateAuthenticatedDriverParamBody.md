# SWGUpdateAuthenticatedDriverParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dplname** | **NSString*** | Display name | [optional] 
**avatar** | **NSString*** | Link hình ảnh avatar | [optional] 
**defaultLocation** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


