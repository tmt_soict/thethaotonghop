# SWGCreateNewAdminParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uname** | **NSString*** | username to login | [optional] 
**password** | **NSString*** | MD5 hashed password | [optional] 
**dplname** | **NSString*** |  | [optional] 
**phone** | **NSString*** |  | [optional] 
**avatar** | **NSString*** | avatar url | [optional] 
**permission** | **NSNumber*** |  | [optional] 
**operatorId** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


