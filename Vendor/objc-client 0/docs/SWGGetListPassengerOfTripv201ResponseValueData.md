# SWGGetListPassengerOfTripv201ResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**NSArray&lt;SWGTicketOfTripv201&gt;***](SWGTicketOfTripv201.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


