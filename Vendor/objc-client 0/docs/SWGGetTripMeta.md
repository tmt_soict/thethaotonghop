# SWGGetTripMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lastId** | **NSNumber*** | id of last item / last date | 
**minSize** | **NSNumber*** | minimum items / request | 
**orderType** | **NSNumber*** | 0 &#x3D; old -&gt; new | 1 &#x3D; nnew -&gt; old | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


