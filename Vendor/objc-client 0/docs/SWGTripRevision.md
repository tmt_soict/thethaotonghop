# SWGTripRevision

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**editor** | **NSNumber*** |  | [optional] 
**time** | **NSNumber*** |  | [optional] 
**tripInfo** | [**SWGTripInfo***](SWGTripInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


