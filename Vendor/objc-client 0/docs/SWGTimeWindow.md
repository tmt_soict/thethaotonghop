# SWGTimeWindow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeRange** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**deportPoint** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**timewindowType** | **NSNumber*** | TimeWindow type | 0 &#x3D; daily | 1 &#x3D; specified time | 
**tripCount** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


