# SWGGetTripHistoryResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta** | [**SWGGetTripMeta***](SWGGetTripMeta.md) |  | [optional] 
**trips** | [**NSArray&lt;SWGTrip&gt;***](SWGTrip.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


