# SWGDriver

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**dplname** | **NSString*** | Display name | 
**avatar** | **NSString*** | Link hình ảnh avatar | 
**phone** | **NSString*** |  | 
**status** | **NSNumber*** | driver status | 0 &#x3D; not activated | 1 &#x3D; activated | 2 &#x3D; banned | 99&#x3D; ghost | 
**defaultLocation** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**vehicle** | [**SWGVehicle***](SWGVehicle.md) |  | 
**operator** | [**SWGOperator***](SWGOperator.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


