# SWGGetBookingResponseValueData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**driver** | [**SWGDriver***](SWGDriver.md) |  | [optional] 
**booking** | [**SWGBooking***](SWGBooking.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


