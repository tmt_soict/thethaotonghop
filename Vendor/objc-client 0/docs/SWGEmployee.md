# SWGEmployee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**role** | **NSNumber*** | 1 &#x3D; DRIVER | 2 &#x3D; ADMIN | 
**uname** | **NSString*** |  | 
**dplname** | **NSString*** |  | 
**phone** | **NSString*** |  | 
**avatar** | **NSString*** | avatar url | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


