# SWGPassenger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**dplname** | **NSString*** | displayname | 
**avatar** | **NSString*** | avatar url | 
**phone** | **NSString*** |  | 
**status** | **NSNumber*** | account status | 0 &#x3D; not activated | 1 &#x3D; activated | 2 &#x3D; banned | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


