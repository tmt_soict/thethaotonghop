#import "SWGRequest.h"

@implementation SWGRequest

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"from": @"from", @"to": @"to", @"request_id": @"requestId", @"pickup_time": @"pickupTime", @"dropoff_time": @"dropoffTime", @"number_passenger": @"numberPassenger", @"is_sharing": @"isSharing", @"request_status": @"requestStatus", @"distance": @"distance", @"price_sum": @"priceSum", @"bookings": @"bookings" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"distance", ];
  return [optionalProperties containsObject:propertyName];
}

@end
