#import "SWGNewStop.h"

@implementation SWGNewStop

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"request_id": @"requestId", @"location": @"location", @"required_pickup_time": @"requiredPickupTime", @"arrive_time": @"arriveTime", @"bookings": @"bookings", @"number_passenger": @"numberPassenger", @"is_shared_request": @"isSharedRequest", @"stop_type": @"stopType" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"requestId", @"location", @"requiredPickupTime", @"arriveTime", @"bookings", @"numberPassenger", @"isSharedRequest", @"stopType"];
  return [optionalProperties containsObject:propertyName];
}

@end
