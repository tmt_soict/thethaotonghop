#import "SWGNewBooking.h"

@implementation SWGNewBooking

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"booking_id": @"bookingId", @"booking_code": @"bookingCode", @"request_id": @"requestId", @"passenger": @"passenger", @"from": @"from", @"to": @"to", @"pickup_time": @"pickupTime", @"estimate_pickup_time": @"estimatePickupTime", @"dropoff_time": @"dropoffTime", @"load": @"load", @"is_sharing": @"isSharing", @"status": @"status", @"distance": @"distance", @"price_range": @"priceRange", @"stop_type_of_booking": @"stopTypeOfBooking" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"bookingId", @"bookingCode", @"passenger", @"from", @"to", @"pickupTime", @"estimatePickupTime", @"dropoffTime", ];
  return [optionalProperties containsObject:propertyName];
}

@end
