#import "SWGTripInfo.h"

@implementation SWGTripInfo

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"driver_id": @"driverId", @"start_time": @"startTime", @"distance": @"distance", @"status": @"status", @"total_passengers": @"totalPassengers", @"income": @"income", @"route": @"route" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"driverId", @"startTime", @"distance", @"status", @"totalPassengers", @"income", @"route"];
  return [optionalProperties containsObject:propertyName];
}

@end
