#import "SWGDriver.h"

@implementation SWGDriver

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"dplname": @"dplname", @"avatar": @"avatar", @"phone": @"phone", @"status": @"status", @"default_location": @"defaultLocation", @"vehicle": @"vehicle", @"operator": @"operator" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"defaultLocation", @"operator"];
  return [optionalProperties containsObject:propertyName];
}

@end
