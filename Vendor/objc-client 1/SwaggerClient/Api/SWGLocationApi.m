#import "SWGLocationApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGDeleteUserLocationResponseValue.h"
#import "SWGGetUserLocationResponseValue.h"
#import "SWGSetUserLocationParamBody.h"
#import "SWGSetUserLocationResponseValue.h"


@interface SWGLocationApi ()

@property (nonatomic, strong) NSMutableDictionary *defaultHeaders;

@end

@implementation SWGLocationApi

NSString* kSWGLocationApiErrorDomain = @"SWGLocationApiErrorDomain";
NSInteger kSWGLocationApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    self = [super init];
    if (self) {
        SWGConfiguration *config = [SWGConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[SWGApiClient alloc] init];
        }
        _apiClient = config.apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+ (instancetype)sharedAPI {
    static SWGLocationApi *sharedAPI;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedAPI = [[self alloc] init];
    });
    return sharedAPI;
}

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.defaultHeaders[key];
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self setDefaultHeaderValue:value forKey:key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(NSUInteger) requestQueueSize {
    return [SWGApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Delete user's location by uid
/// Delete user's location by uid
///  @param userId  
///
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGDeleteUserLocationResponseValue*
///
-(NSNumber*) deleteUserLocationByUIDWithUserId: (NSNumber*) userId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGDeleteUserLocationResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'userId' is set
    if (userId == nil) {
        NSParameterAssert(userId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"userId"] };
            NSError* error = [NSError errorWithDomain:kSWGLocationApiErrorDomain code:kSWGLocationApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/locations/{user_id}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (userId != nil) {
        pathParams[@"user_id"] = userId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"DELETE"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGDeleteUserLocationResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGDeleteUserLocationResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// Get user's location by uid
/// Get user's location by uid
///  @param userId  
///
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @returns SWGGetUserLocationResponseValue*
///
-(NSNumber*) getUserLocationByUIDWithUserId: (NSNumber*) userId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    completionHandler: (void (^)(SWGGetUserLocationResponseValue* output, NSError* error)) handler {
    // verify the required parameter 'userId' is set
    if (userId == nil) {
        NSParameterAssert(userId);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"userId"] };
            NSError* error = [NSError errorWithDomain:kSWGLocationApiErrorDomain code:kSWGLocationApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/locations/{user_id}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (userId != nil) {
        pathParams[@"user_id"] = userId;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGGetUserLocationResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGGetUserLocationResponseValue*)data, error);
                                }
                           }
          ];
}

///
/// User set location
/// User set location
///  @param sessionkey  (optional)
///
///  @param aver API version (optional, default to 2.0.0)
///
///  @param ctype Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
///
///  @param body  (optional)
///
///  @returns SWGSetUserLocationResponseValue*
///
-(NSNumber*) setUserLocationWithSessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGSetUserLocationParamBody*) body
    completionHandler: (void (^)(SWGSetUserLocationResponseValue* output, NSError* error)) handler {
    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/locations"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (sessionkey != nil) {
        headerParams[@"Sessionkey"] = sessionkey;
    }
    if (aver != nil) {
        headerParams[@"Aver"] = aver;
    }
    if (ctype != nil) {
        headerParams[@"Ctype"] = ctype;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];
    bodyParam = body;

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"POST"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGSetUserLocationResponseValue*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGSetUserLocationResponseValue*)data, error);
                                }
                           }
          ];
}



@end
