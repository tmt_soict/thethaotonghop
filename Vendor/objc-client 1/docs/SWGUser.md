# SWGUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**dplname** | **NSString*** | displayname | 
**phone** | **NSString*** |  | 
**status** | **NSNumber*** | user status | 0 &#x3D; not activated | 1 &#x3D; activated | 2 &#x3D; banned | 
**sessionkey** | **NSString*** | sessionkey to authenticated with api | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


