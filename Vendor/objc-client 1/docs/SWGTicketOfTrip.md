# SWGTicketOfTrip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contact** | [**SWGPassenger***](SWGPassenger.md) |  | 
**startTime** | **NSNumber*** |  | 
**fromAddress** | **NSString*** |  | 
**toAddress** | **NSString*** |  | 
**numberPassenger** | **NSNumber*** |  | 
**price** | [**SWGCostRange***](SWGCostRange.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


