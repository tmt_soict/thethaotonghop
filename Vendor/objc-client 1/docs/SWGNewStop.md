# SWGNewStop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **NSNumber*** |  | [optional] 
**location** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**requiredPickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**arriveTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**bookings** | [**NSArray&lt;SWGNewBooking&gt;***](SWGNewBooking.md) |  | [optional] 
**numberPassenger** | **NSNumber*** |  | [optional] 
**isSharedRequest** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


