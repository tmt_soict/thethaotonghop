# SWGLocationPoint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **NSNumber*** | latitude | 
**lng** | **NSNumber*** | longitude | 
**address** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


