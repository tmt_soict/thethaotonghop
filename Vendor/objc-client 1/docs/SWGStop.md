# SWGStop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestId** | **NSNumber*** |  | 
**location** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | 
**requiredPickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**arriveTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | 
**stopType** | **NSNumber*** | 1 &#x3D; pickup | 2 &#x3D; dropoff | 
**passengers** | [**NSArray&lt;SWGPassenger&gt;***](SWGPassenger.md) |  | 
**numberPassenger** | **NSNumber*** |  | 
**isSharedRequest** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


