# SWGSchedule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**day** | **NSNumber*** | begin time of day | 0h:0m:0s | 
**timewindows** | [**NSArray&lt;SWGTimeWindow&gt;***](SWGTimeWindow.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


