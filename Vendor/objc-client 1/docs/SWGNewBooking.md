# SWGNewBooking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookingId** | **NSNumber*** |  | [optional] 
**bookingCode** | **NSString*** |  | [optional] 
**requestId** | **NSNumber*** |  | 
**passenger** | [**SWGPassenger***](SWGPassenger.md) |  | [optional] 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**pickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**estimatePickupTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**dropoffTime** | [**SWGTimeRange***](SWGTimeRange.md) |  | [optional] 
**load** | **NSNumber*** | number of passengers | 
**isSharing** | **NSNumber*** | accept arriving with another passenger or not | 
**stopType** | **NSNumber*** | 1 &#x3D; pickup | 2 &#x3D; dropoff | [optional] 
**status** | **NSNumber*** | booking status PENDING &#x3D; 0 | RUNNING &#x3D; 10 | FINISHED &#x3D; 15 | CANCELED &#x3D; 16 | 
**distance** | **NSNumber*** |  | 
**priceRange** | [**SWGCostRange***](SWGCostRange.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


