# SWGBookingApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBooking**](SWGBookingApi.md#getbooking) | **GET** /bookings/{booking_id} | Get a single booking


# **getBooking**
```objc
-(NSNumber*) getBookingWithBookingId: (NSNumber*) bookingId
    sessionkey: (NSString*) sessionkey
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetBookingResponseValue* output, NSError* error)) handler;
```

Get a single booking

Get a songle booking

### Example 
```objc

NSNumber* bookingId = @789; // 
NSString* sessionkey = @"sessionkey_example"; //  (optional)
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGBookingApi*apiInstance = [[SWGBookingApi alloc] init];

// Get a single booking
[apiInstance getBookingWithBookingId:bookingId
              sessionkey:sessionkey
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetBookingResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGBookingApi->getBooking: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bookingId** | **NSNumber***|  | 
 **sessionkey** | **NSString***|  | [optional] 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetBookingResponseValue***](SWGGetBookingResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

