# SWGOTPApi

All URIs are relative to *http://dev.api.123xe.vn/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOTP**](SWGOTPApi.md#getotp) | **GET** /otp/{phone} | Get otp code by phone.
[**verifyAccount**](SWGOTPApi.md#verifyaccount) | **POST** /otp/{phone} | Verify account by otp code


# **getOTP**
```objc
-(NSNumber*) getOTPWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
        completionHandler: (void (^)(SWGGetOTPResponseValue* output, NSError* error)) handler;
```

Get otp code by phone.

Get otp code by phone

### Example 
```objc

NSString* phone = @"phone_example"; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)

SWGOTPApi*apiInstance = [[SWGOTPApi alloc] init];

// Get otp code by phone.
[apiInstance getOTPWithPhone:phone
              aver:aver
              ctype:ctype
          completionHandler: ^(SWGGetOTPResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGOTPApi->getOTP: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 

### Return type

[**SWGGetOTPResponseValue***](SWGGetOTPResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **verifyAccount**
```objc
-(NSNumber*) verifyAccountWithPhone: (NSString*) phone
    aver: (NSString*) aver
    ctype: (NSNumber*) ctype
    body: (SWGVerifyAccountParamBody*) body
        completionHandler: (void (^)(SWGVerifyAccountResponseValue* output, NSError* error)) handler;
```

Verify account by otp code

Verify account by otp code

### Example 
```objc

NSString* phone = @"phone_example"; // 
NSString* aver = @"2.0.0"; // API version (optional) (default to 2.0.0)
NSNumber* ctype = @56; // Client type | 0 = passenger client | 1 = driver client | 2 = GroupAdmin client | 100 = supper admin (optional)
SWGVerifyAccountParamBody* body = [[SWGVerifyAccountParamBody alloc] init]; //  (optional)

SWGOTPApi*apiInstance = [[SWGOTPApi alloc] init];

// Verify account by otp code
[apiInstance verifyAccountWithPhone:phone
              aver:aver
              ctype:ctype
              body:body
          completionHandler: ^(SWGVerifyAccountResponseValue* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling SWGOTPApi->verifyAccount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **NSString***|  | 
 **aver** | **NSString***| API version | [optional] [default to 2.0.0]
 **ctype** | **NSNumber***| Client type | 0 &#x3D; passenger client | 1 &#x3D; driver client | 2 &#x3D; GroupAdmin client | 100 &#x3D; supper admin | [optional] 
 **body** | [**SWGVerifyAccountParamBody***](SWGVerifyAccountParamBody*.md)|  | [optional] 

### Return type

[**SWGVerifyAccountResponseValue***](SWGVerifyAccountResponseValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

