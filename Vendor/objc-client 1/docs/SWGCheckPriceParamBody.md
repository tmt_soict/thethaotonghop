# SWGCheckPriceParamBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance** | **NSNumber*** |  | [optional] 
**load** | **NSNumber*** |  | [optional] 
**time** | **NSNumber*** |  | [optional] 
**from** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 
**to** | [**SWGLocationPoint***](SWGLocationPoint.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


