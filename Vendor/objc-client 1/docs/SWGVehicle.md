# SWGVehicle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** | id của xe | [optional] 
**vehicleType** | **NSNumber*** | loại xe 4 chỗ, 7 chỗ, 16 chỗ | 
**licensePlate** | **NSString*** | biển số xe. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


