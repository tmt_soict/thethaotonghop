# SWGCostRange

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lowerCost** | **NSNumber*** |  | 
**higherCost** | **NSNumber*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


