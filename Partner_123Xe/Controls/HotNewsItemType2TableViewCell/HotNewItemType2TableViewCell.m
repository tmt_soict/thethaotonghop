//
//  HotNewItemType2TableViewCell.m
//  Partner_123Xe
//
//  Created by TuTMT on 10/22/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "HotNewItemType2TableViewCell.h"

@implementation HotNewItemType2TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setUpHotNewsItemType1TableViewCellWithLinfos: (Linfos*) linfos{
    self.titleLable.text = linfos.title;
    [self.titleImage sd_setImageWithURL:[NSURL URLWithString:linfos.image]
                       placeholderImage:[UIImage imageNamed:@"test_ic"]];
    double remainTime = [[NSDate date] timeIntervalSince1970] - linfos.posttime;
    if (remainTime <= 0 ) {
        _timeLable.text = @"0 phút trước";
    }
    else{
        
    }
    
}

@end
