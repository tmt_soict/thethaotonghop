//
//  HotNewItemType2TableViewCell.h
//  Partner_123Xe
//
//  Created by TuTMT on 10/22/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HotNewItemType2TableViewCellDelegate<NSObject>

@end


@interface HotNewItemType2TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
-(void)setUpHotNewsItemType1TableViewCellWithLinfos: (Linfos*) linfos;
@end
