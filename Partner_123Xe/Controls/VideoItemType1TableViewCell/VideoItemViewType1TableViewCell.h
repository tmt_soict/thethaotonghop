//
//  VideoItemViewType1TableViewCell.h
//  SportsNews
//
//  Created by TuTMT on 10/27/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol VideoItemViewType1TableViewCellDelegate<NSObject>
-(void)playVideoButtonTouchUpInside;
@end

@interface VideoItemViewType1TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@property (nonatomic,assign) id<VideoItemViewType1TableViewCellDelegate> delegate;



- (IBAction)playButtonTouchUpInside:(id)sender;
-(void)setUpHotNewsItemType1TableViewCellWithLinfos: (Linfos*) linfos;


@end
