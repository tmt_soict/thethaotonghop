//
//  TrangBaoItemCollectionViewCell.h
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrangBaoItemCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *BaoImage;
@property (weak, nonatomic) IBOutlet UILabel *nameBaoLabel;

@end
