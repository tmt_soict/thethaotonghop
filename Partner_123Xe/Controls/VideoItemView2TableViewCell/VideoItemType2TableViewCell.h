//
//  VideoItemType2TableViewCell.h
//  SportsNews
//
//  Created by TuTMT on 10/27/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol VideoItemType2TableViewCellDelegate<NSObject>
@end

@interface VideoItemType2TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *timeLable;
-(void)setUpHotNewsItemType1TableViewCellWithLinfos: (Linfos*) linfos;

@end
