//
//  NetworkManager.m
//  Pincharts
//
//  Created by MAC on 11/14/13.
//
//

#import "NetworkManager.h"

#import "Util.h"

@implementation NetworkManager

+(NetworkManager*)shareNetworkManager
{
    static NetworkManager* shareInstance;
    static dispatch_once_t onceTaken;
    dispatch_once(&onceTaken,^{
        shareInstance = [[NetworkManager alloc] init];
    });
    return shareInstance;
}

#pragma mark - Network Status

-(void)setupReachability
{
    if (_isSetup)
        return;
    else
        _isSetup = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    // check if a pathway to a random host exists
    //self.hostReach = [Reachability reachabilityWithHostName: @"www.google.com"] ;
    self.hostReach = [Reachability reachabilityForLocalWiFi];
    [_hostReach startNotifier];
    
    //Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
	//[internetReach startNotifier];
    
    [self addNetWorkLabel];
    [self checkNetworkStatus:nil];
}

-(void)addNetWorkLabel
{
    UIViewController *rootViewController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
    
    float alpha = 0;
    if(statusNetworkLabel) alpha = statusNetworkLabel.alpha;
    
    statusNetworkLabel  = [[UILabel alloc] initWithFrame:CGRectMake(0, 64, 320, 30)];
    statusNetworkLabel.textAlignment = NSTextAlignmentCenter;
    statusNetworkLabel.textColor = [UIColor whiteColor];
    statusNetworkLabel.text = @"Kết nối mạng không ổn định";
    statusNetworkLabel.alpha = alpha;
    hasNetwork = YES;
    [statusNetworkLabel setBackgroundColor:[UIColor colorWithRed:0.8 green:0 blue:0 alpha:0.9]];
    [rootViewController.view addSubview:statusNetworkLabel];
    statusNetworkLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
}

-(void) checkNetworkStatus:(NSNotification *)notice
{
    Reachability* curReach ;
    if(notice != nil)
        curReach = [notice object];
    else
        curReach = _hostReach;
    NetworkStatus hostStatus = [curReach currentReachabilityStatus];
    hasNetwork = YES;
    switch (hostStatus)
    {
        case NotReachable:
        {
            NSLog(@"A gateway to the host server is down.");
            hasNetwork = NO;
            [Util showMessage:@"Hãy kết nối internet để có thể sử dụng hết các tính năng của ứng dụng." withTitle:@"Thông báo"];
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"A gateway to the host server is working via WIFI.");
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"A gateway to the host server is working via WWAN.");
            break;
        }
    }
    
    
    if(hasNetwork==NO)
    {
        [UIView animateWithDuration:0.5 animations:^{
            statusNetworkLabel.alpha = 1;
        }];
    }else{
        [UIView animateWithDuration:0.5 animations:^{
            statusNetworkLabel.alpha = 0;
        }];
    }
    
}


@end
