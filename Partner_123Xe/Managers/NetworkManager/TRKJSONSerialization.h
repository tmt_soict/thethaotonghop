//
//  TRKJSONSerialization.h
//  paymentclient
//
//  Created by Nguyễn Hữu Hoà on 12/25/15.
//  Copyright © 2015 VNG Corporation. All rights reserved.
//

#import <RestKitSANetworking@MindSea/RestKit.h>

@interface TRKJSONSerialization : NSObject <RKSerialization>

@end
