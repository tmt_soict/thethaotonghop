//
//  NetworkManager.h
//  Pincharts
//
//  Created by MAC on 11/14/13.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <Reachability/Reachability.h>

@interface NetworkManager : NSObject
{
    
    UILabel* statusNetworkLabel;
    BOOL hasNetwork;
    BOOL _isSetup;
}

@property (nonatomic, strong) Reachability* hostReach;

+(NetworkManager*)shareNetworkManager;
-(void)setupReachability;
-(void)addNetWorkLabel;
@end
