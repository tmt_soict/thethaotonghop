//
//  TRKJSONSerialization.m
//  paymentclient
//
//  Created by Nguyễn Hữu Hoà on 12/25/15.
//  Copyright © 2015 VNG Corporation. All rights reserved.
//

#import "TRKJSONSerialization.h"

@implementation TRKJSONSerialization

+ (id)objectFromData:(NSData *)data error:(NSError **)error
{
    DLog(@"Response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
}

+ (NSData *)dataFromObject:(id)object error:(NSError **)error
{
    DLog(@"Request: %@", object);
    return [NSJSONSerialization dataWithJSONObject:object options:0 error:error];
}

@end
