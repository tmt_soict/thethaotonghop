//
//  BaseManager.h
//  paymentclient
//
//  Created by PhucPv on 1/14/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ResponseObject;
@interface BaseManager : NSObject
- (NSString *)phoneNumber;
- (NSString *)shortSession;
- (NSString *)longSession;
//- (void)isSystemError:(ResponseObject *)responseObject
//              message:(NSString **)message;
@end
