//
//  BaseManager.m
//  paymentclient
//
//  Created by PhucPv on 1/14/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "BaseManager.h"
#import "AppData.h"
#import "APIClient.h"
#import "ResponseObject.h"
@implementation BaseManager
- (NSString *)phoneNumber {
    return [[AppData sharedInstance] phoneNumber];
}

//
//- (void)isSystemError:(ResponseObject *)responseObject
//              message:(NSString **)message {
//    if (responseObject.errorCode == API_SYSTEM_ERROR || responseObject.errorCode ==  SYSTEM_ERROR) {
//        *message = @"Lỗi hệ thống";
//    } else if (!*message) {
//        if (responseObject.message) {
//            *message = responseObject.message;
//        } else {
//            *message = [NSString stringWithFormat:@"Error code: %i", responseObject.errorCode];
//        }
//    }
//}

@end
