//
//  Linfos.m
//
//  Created by TuTMT  on 10/31/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Linfos.h"


NSString *const kLinfosLid = @"lid";
NSString *const kLinfosContent = @"content";
NSString *const kLinfosPosttime = @"posttime";
NSString *const kLinfosListimage = @"listimage";
NSString *const kLinfosCid = @"cid";
NSString *const kLinfosTitle = @"title";
NSString *const kLinfosImage = @"image";
NSString *const kLinfosUrl = @"url";
NSString *const kLinfosSid = @"sid";
NSString *const kLinfosDesc = @"desc";
NSString *const kLinfosSmallThumbnail = @"small_thumbnail";


@interface Linfos ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Linfos

@synthesize lid = _lid;
@synthesize content = _content;
@synthesize posttime = _posttime;
@synthesize listimage = _listimage;
@synthesize cid = _cid;
@synthesize title = _title;
@synthesize image = _image;
@synthesize url = _url;
@synthesize sid = _sid;
@synthesize desc = _desc;
@synthesize smallThumbnail = _smallThumbnail;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.lid = [[self objectOrNilForKey:kLinfosLid fromDictionary:dict] doubleValue];
            self.content = [self objectOrNilForKey:kLinfosContent fromDictionary:dict];
            self.posttime = [[self objectOrNilForKey:kLinfosPosttime fromDictionary:dict] doubleValue];
            self.listimage = [self objectOrNilForKey:kLinfosListimage fromDictionary:dict];
            self.cid = [[self objectOrNilForKey:kLinfosCid fromDictionary:dict] doubleValue];
            self.title = [self objectOrNilForKey:kLinfosTitle fromDictionary:dict];
            self.image = [self objectOrNilForKey:kLinfosImage fromDictionary:dict];
            self.url = [self objectOrNilForKey:kLinfosUrl fromDictionary:dict];
            self.sid = [[self objectOrNilForKey:kLinfosSid fromDictionary:dict] doubleValue];
            self.desc = [self objectOrNilForKey:kLinfosDesc fromDictionary:dict];
            self.smallThumbnail = [self objectOrNilForKey:kLinfosSmallThumbnail fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.lid] forKey:kLinfosLid];
    [mutableDict setValue:self.content forKey:kLinfosContent];
    [mutableDict setValue:[NSNumber numberWithDouble:self.posttime] forKey:kLinfosPosttime];
    NSMutableArray *tempArrayForListimage = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.listimage) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForListimage addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForListimage addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForListimage] forKey:kLinfosListimage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.cid] forKey:kLinfosCid];
    [mutableDict setValue:self.title forKey:kLinfosTitle];
    [mutableDict setValue:self.image forKey:kLinfosImage];
    [mutableDict setValue:self.url forKey:kLinfosUrl];
    [mutableDict setValue:[NSNumber numberWithDouble:self.sid] forKey:kLinfosSid];
    [mutableDict setValue:self.desc forKey:kLinfosDesc];
    [mutableDict setValue:self.smallThumbnail forKey:kLinfosSmallThumbnail];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.lid = [aDecoder decodeDoubleForKey:kLinfosLid];
    self.content = [aDecoder decodeObjectForKey:kLinfosContent];
    self.posttime = [aDecoder decodeDoubleForKey:kLinfosPosttime];
    self.listimage = [aDecoder decodeObjectForKey:kLinfosListimage];
    self.cid = [aDecoder decodeDoubleForKey:kLinfosCid];
    self.title = [aDecoder decodeObjectForKey:kLinfosTitle];
    self.image = [aDecoder decodeObjectForKey:kLinfosImage];
    self.url = [aDecoder decodeObjectForKey:kLinfosUrl];
    self.sid = [aDecoder decodeDoubleForKey:kLinfosSid];
    self.desc = [aDecoder decodeObjectForKey:kLinfosDesc];
    self.smallThumbnail = [aDecoder decodeObjectForKey:kLinfosSmallThumbnail];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_lid forKey:kLinfosLid];
    [aCoder encodeObject:_content forKey:kLinfosContent];
    [aCoder encodeDouble:_posttime forKey:kLinfosPosttime];
    [aCoder encodeObject:_listimage forKey:kLinfosListimage];
    [aCoder encodeDouble:_cid forKey:kLinfosCid];
    [aCoder encodeObject:_title forKey:kLinfosTitle];
    [aCoder encodeObject:_image forKey:kLinfosImage];
    [aCoder encodeObject:_url forKey:kLinfosUrl];
    [aCoder encodeDouble:_sid forKey:kLinfosSid];
    [aCoder encodeObject:_desc forKey:kLinfosDesc];
    [aCoder encodeObject:_smallThumbnail forKey:kLinfosSmallThumbnail];
}

- (id)copyWithZone:(NSZone *)zone {
    Linfos *copy = [[Linfos alloc] init];
    
    
    
    if (copy) {

        copy.lid = self.lid;
        copy.content = [self.content copyWithZone:zone];
        copy.posttime = self.posttime;
        copy.listimage = [self.listimage copyWithZone:zone];
        copy.cid = self.cid;
        copy.title = [self.title copyWithZone:zone];
        copy.image = [self.image copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.sid = self.sid;
        copy.desc = [self.desc copyWithZone:zone];
        copy.smallThumbnail = [self.smallThumbnail copyWithZone:zone];
    }
    
    return copy;
}


@end
