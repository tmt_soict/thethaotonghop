//
//  Data.m
//
//  Created by TuTMT  on 10/31/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "Data.h"
#import "Linfos.h"


NSString *const kDataLids = @"lids";
NSString *const kDataLinfos = @"linfos";


@interface Data ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Data

@synthesize lids = _lids;
@synthesize linfos = _linfos;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.lids = [self objectOrNilForKey:kDataLids fromDictionary:dict];
    NSObject *receivedLinfos = [dict objectForKey:kDataLinfos];
    NSMutableArray *parsedLinfos = [NSMutableArray array];
    
    if ([receivedLinfos isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedLinfos) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedLinfos addObject:[Linfos modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedLinfos isKindOfClass:[NSDictionary class]]) {
       [parsedLinfos addObject:[Linfos modelObjectWithDictionary:(NSDictionary *)receivedLinfos]];
    }

    self.linfos = [NSArray arrayWithArray:parsedLinfos];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForLids = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.lids) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForLids addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForLids addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForLids] forKey:kDataLids];
    NSMutableArray *tempArrayForLinfos = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.linfos) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForLinfos addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForLinfos addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForLinfos] forKey:kDataLinfos];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.lids = [aDecoder decodeObjectForKey:kDataLids];
    self.linfos = [aDecoder decodeObjectForKey:kDataLinfos];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_lids forKey:kDataLids];
    [aCoder encodeObject:_linfos forKey:kDataLinfos];
}

- (id)copyWithZone:(NSZone *)zone {
    Data *copy = [[Data alloc] init];
    
    
    
    if (copy) {

        copy.lids = [self.lids copyWithZone:zone];
        copy.linfos = [self.linfos copyWithZone:zone];
    }
    
    return copy;
}


@end
