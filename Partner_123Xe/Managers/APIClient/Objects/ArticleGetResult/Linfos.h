//
//  Linfos.h
//
//  Created by TuTMT  on 10/31/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Linfos : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double lid;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, assign) double posttime;
@property (nonatomic, strong) NSArray *listimage;
@property (nonatomic, assign) double cid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) double sid;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *smallThumbnail;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
