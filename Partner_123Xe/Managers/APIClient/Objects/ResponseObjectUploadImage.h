//
//  ResponseObjectUploadImage.h
//  PaymentUbusPartner
//
//  Created by QuangKomodo on 12/04/2016.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResponseObjectUploadImage : NSObject
@property (nonatomic, assign) int errorCode;
@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) NSNumber *photoId;
@property (nonatomic, strong) NSString *photoUrl;
@end
