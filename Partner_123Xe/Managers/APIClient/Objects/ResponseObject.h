//
//  ResponseObject.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//  
//

#import <Foundation/Foundation.h>

@interface ResponseObject : NSObject

@property (nonatomic) id result;
@property (nonatomic, assign) int errorCode;
@property (nonatomic, strong) NSString * message;

@end
