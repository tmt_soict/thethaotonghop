//
//  APIClient.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import <UIKit/UIKit.h>
#import <RestKitSANetworking@MindSea/RestKit.h>
#import "SAHTTPClient.h"
#import "SAImageRequestOperation.h"
#import "ResponseObject.h"
#import "ResponseObjectUploadImage.h"
#import "DataModels.h"

#define BASE_URI            @"http://api.5play.mobi/news/v0.1/articles?"

@interface APIClient : SAHTTPClient

#pragma mark- Public methods

+ (APIClient *)sharedClient;

//Define all API functions here

#pragma mark- I. Session
#pragma mark- 1.1. Register

- (void)registerWithUsername:(NSString *)username
                    password:(NSString *)password
                       block:(void (^)(ResponseObject * responseObject))block;


#pragma mark- get Article 
-(void)getArticleWithSid:(NSString*)sid
                     cid:(NSString*) cid
                   count:(NSNumber*)count
                    meta:(BOOL) meta
                 lastLid:(NSNumber*) lid
       completionHandler: (void (^)(Data* output, NSError* error, double  errorCode)) handler;
#pragma mark- test 
//-(void) uploadTestWithData:(NSString*) name;

@end
