//
//  APIClient.m
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import "APIClient.h"
#import "Global.h"
#import "URLConstant.h"
#import "AppData.h"
#import "AppDelegate.h"

#define API_VERSION @"1.01"
#define API_VERSION_KEY @"api_ver"
#define SESSION_KEY @"sessionkey"

// Objects


#import "ResponseObjectUploadImage.h"

@implementation APIClient

#pragma mark- Public methods

+ (APIClient *)sharedClient {
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[SAHTTPRequestOperation class]];
    
    return self;
}

#pragma -mark get article 
-(void)getArticleWithSid:(NSString*)sid
cid:(NSString*) cid
count:(NSString*)count
meta:(BOOL) meta
lastLid:(NSNumber *)lid
       completionHandler: (void (^)(Data* output, NSError* error, double  errorCode)) handler{
    NSString* urlRequest ;
    if (meta) {
        urlRequest =  [NSString stringWithFormat:@"%@sid=%@&cid=%@&count=%@&meta=0&lid=%@",BASE_URI,sid,cid,count,lid];
    }
    else{
        urlRequest =  [NSString stringWithFormat:@"%@sid=%@&cid=%@&count=%@&lid=%@",BASE_URI,sid,cid,count,lid];

    }
    //test url
    //urlRequest = @"http://api.5play.mobi/news/v0.1/articles?sid=999&cid=999&count=30&meta=0";
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlRequest parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                //blah blah
                if (handler) {
                    Data *data  = [[Data alloc] initWithDictionary:responseObject[@"data"]];
                    NSNumber* code = (NSNumber*) responseObject[@"code"];
                    handler(data,error, [code doubleValue]);
                }
            }
        } else {
            if (handler) {
                handler(nil,error,0);
            }
        }
    }] resume];
    
  
}


@end
