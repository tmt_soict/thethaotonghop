//
//  ArrayData.m
//  PaymentUbusPartner
//
//  Created by QuangKomodo on 22/03/2016.
//  Copyright © Năm 2016 VNG Corporation. All rights reserved.
//

#import "ArrayData.h"

#define SCHEDULED_DATES_KEY @"scheduled_dates"


@implementation ArrayData
#pragma mark- Storage utility

static ArrayData  *sharedObject = nil;
+ (ArrayData *)sharedInstance
{
    if (sharedObject == nil) {
        @synchronized(self){
            if (sharedObject == nil) {
                sharedObject = [[ArrayData alloc] init];
            }
        }
    }
    return sharedObject;
}

- (void)writeArrayWithCustomObjToUserDefaults:(NSString *)keyName withArray:(NSMutableArray *)myArray
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:myArray];
    [defaults setObject:data forKey:keyName];
    [defaults synchronize];
}

- (NSMutableArray *)readArrayWithCustomObjFromUserDefaults:(NSString*)keyName
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:keyName];
    NSArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    [defaults synchronize];
    if(myArray)
    {
        return [[NSMutableArray alloc] initWithArray:myArray];
    }
    else
    {
        return nil;
    }
}

#pragma mark- Public methods

- (void)resetData
{
    //NSMutableArray * emptyArray = [[NSMutableArray alloc] init];
}

-(void)setScheduledDates:(NSMutableArray *)scheduledDates{
    [self writeArrayWithCustomObjToUserDefaults:SCHEDULED_DATES_KEY withArray:scheduledDates];
}
-(NSMutableArray *)scheduledDates{
    return [self readArrayWithCustomObjFromUserDefaults:SCHEDULED_DATES_KEY];
}

@end
