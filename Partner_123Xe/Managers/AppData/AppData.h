//
//  AppData.h
//  Shoppie
//
//  Created by tinvukhac on 11/14/13.
//
//

#import <Foundation/Foundation.h>

@class SessionObject;

@interface AppData : NSObject

#pragma mark- App data methods
+ (AppData*)sharedInstance;

- (BOOL)isAlreadyExistKey:(NSString *)key;

- (void)updateOrCreateNewValue:(id)value forKey:(NSString *)key;

- (id)getValueForKey:(NSString *)key;

#pragma mark- Public methods

- (void)resetData;

#pragma mark- App data properties
/**
 * Nơi lưu trữ các thông tin dạng key-value của ứng dụng.
 * Qui tắc tạo thuộc tính mới:
 * - Khai báo hai hàm get-set của thuộc tính.
 * - Implement trong file .m
 */

// Property check user is logged in or not
@property BOOL                                  loggedIn;

// Phone number of current user
@property (nonatomic, retain) NSString          *phoneNumber;

// Session Key
@property (nonatomic, retain) NSString          *sessionKey;

// User Id
@property (nonatomic, retain) NSNumber          *userId;


// First name
@property (nonatomic, retain) NSString          *name;

// Password
@property (nonatomic, retain) NSString          *password;

// Email
@property (nonatomic, retain) NSString          *email;

// Display name
@property (nonatomic, retain) NSString          *displayName;

// Avatar
@property (nonatomic, retain) NSString          *avatar;


@property (nonatomic, retain) NSString          *error;

// Device token
@property (nonatomic, retain) NSString          *deviceToken;

// Last registered device token
@property (nonatomic, retain) NSString          *lastRegisteredDeviceToken;
@property (nonatomic, retain) NSString          *deviceId;

//lid
@property (nonatomic, retain) NSString          *lid;

//link article
@property (nonatomic, retain) NSString          *url;

//cid
@property (nonatomic,retain)  NSNumber           *cid;

//sid
@property (nonatomic,retain) NSNumber           *sid;

//title
@property (nonatomic,retain) NSString           *title;

//desc
@property (nonatomic,retain) NSNumber           *posttime;

//image
@property (nonatomic,retain) NSString           *image;

//desc
@property (nonatomic,retain) NSString           *desc;


//content
@property (nonatomic,retain) NSString           *content;


-(NSString *)photoUrl;

@end
