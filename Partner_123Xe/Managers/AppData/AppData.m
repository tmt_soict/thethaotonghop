//
//  AppData.m
//  Shoppie
//
//  Created by tinvukhac on 11/14/13.
//
//

#import "AppData.h"
#import "ResponseObject.h"

@implementation AppData

#pragma mark- App data methods

+ (AppData*)sharedInstance {
    static AppData  *sharedObject = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedObject = [[AppData alloc] init];
    });
    return sharedObject;
}

- (BOOL)isAlreadyExistKey:(NSString *)key
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:key])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)updateOrCreateNewValue:(id)value forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getValueForKey:(NSString *)key
{
    if([self isAlreadyExistKey:key])
    {
        return [[NSUserDefaults standardUserDefaults] valueForKey:key];
    }
    else
    {
        return nil;
    }
}

#pragma mark- Public methods

- (void)resetData
{
    [self setPhoneNumber:@""];
    [self setName:@""];
    [self setPassword:@""];
    [self setEmail:@""];
    [self setDisplayName:@""];
    [self setUserId:[NSNumber numberWithInt:0]];
}

#pragma mark- App data properties

// Phone number of current user
- (void)setPhoneNumber:(NSString *)phoneNumber
{
    [self updateOrCreateNewValue:phoneNumber forKey:@"phoneNumber"];
}

- (NSString*)phoneNumber
{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"phoneNumber"]];
}


// User Id

- (void)setUserId:(NSNumber *)userId
{
    [self updateOrCreateNewValue:userId forKey:@"userId"];
}

- (NSNumber *)userId
{
    return [self getValueForKey:@"userId"];
}

// ERROR

-(void) setError:(NSString *)error
{
    [self updateOrCreateNewValue:error forKey:@"error"];
}
-(NSString *) error{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"error"]];
}

// Password
- (void)setPassword:(NSString *)password
{
    [self updateOrCreateNewValue:password forKey:@"password"];
}

- (NSString *)password
{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"password"]];
}

// Email
- (void)setEmail:(NSString *)email
{
    [self updateOrCreateNewValue:email forKey:@"email"];
}

- (NSString *)email
{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"email"]];
}

// Display name
- (void)setDisplayName:(NSString *)displayName
{
    [self updateOrCreateNewValue:displayName forKey:@"displayName"];
}

- (NSString *)displayName
{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"displayName"]];
}


// Avatar
- (void)setAvatar:(NSString *)avatar
{
    [self updateOrCreateNewValue:avatar forKey:@"avatar"];
}

- (NSString *)avatar
{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"avatar"]];
}

// dplname
- (void)setDplname:(NSString *)dplname
{
    [self updateOrCreateNewValue:dplname forKey:@"dplname"];
}

- (NSString *)dplname
{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"dplname"]];
}


//lid
-(void)setLid:(NSString *)lid{
    [self updateOrCreateNewValue:lid forKey:@"lid"];
}
-(NSString*)lid{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"lid"]];
}

//cid
-(void)setCid:(NSNumber *)cid{
    [self updateOrCreateNewValue:cid forKey:@"cid"];

}
-(NSNumber*)cid{
    return [self getValueForKey:@"cid"];
}


//sid
-(void)setSid:(NSNumber *)sid{
    [self updateOrCreateNewValue:sid forKey:@"sid"];

}
-(NSNumber*)sid{
    return [self getValueForKey:@"sid"];

}

//url
-(void)setUrl:(NSString *)url{
    [self updateOrCreateNewValue:url forKey:@"url"];
}
-(NSString*)url{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"url"]];
}

//title
-(void)setTitle:(NSString *)title{
    [self updateOrCreateNewValue:title forKey:@"title"];

}
-(NSString*)title{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"title"]];
}

//desc
-(void)setDesc:(NSString *)desc{
    [self updateOrCreateNewValue:desc forKey:@"desc"];
    
}
-(NSString*)desc{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"desc"]];
}

//postime
-(void)setPosttime:(NSNumber *)posttime{
    [self updateOrCreateNewValue:posttime forKey:@"posttime"];
    
}
-(NSNumber*)posttime{
    return [self getValueForKey:@"posttime"];
    
}

//image
-(void)setImage:(NSString *)image{
    [self updateOrCreateNewValue:image forKey:@"image"];
    
}
-(NSString*)image{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"image"]];
}

//content
-(void)setContent:(NSString *)content{
    [self updateOrCreateNewValue:content forKey:@"content"];
    
}
-(NSString*)content{
    return [self returnNilWhenValueEmpty:[self getValueForKey:@"content"]];
}


- (NSString *)returnNilWhenValueEmpty:(id)value {
    if (!value||[value isEqualToString:@""]) {
        return nil;
    }
    return value;
}


@end












