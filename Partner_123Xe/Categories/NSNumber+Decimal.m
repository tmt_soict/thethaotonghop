//
//  NSNumber+Decimal.m
//  paymentclient
//
//  Created by Nguyễn Hữu Hoà on 1/20/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "NSNumber+Decimal.h"

@implementation NSNumber (Decimal)
+ (NSNumberFormatter*) currencyNumberFormatter {
    static NSNumberFormatter* formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setUsesSignificantDigits:YES];
        [formatter setMinimumSignificantDigits:0];
        [formatter setGroupingSeparator:@","];
        [formatter setDecimalSeparator:@"."];
    });
    
    return formatter;
}

- (NSString *)formatCurrency {
    NSString * formattedAmount = [[NSNumber currencyNumberFormatter] stringFromNumber:self];
    return [formattedAmount stringByAppendingFormat:@" %@", @"VND"];
}

@end
