//
//  UIColor+Extension.m
//  NewsReader
//
//  Created by Phuc  Pham Van on 3/10/14.
//  Copyright (c) 2014 NP. All rights reserved.
//
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

#import "UIColor+Extension.h"

@implementation UIColor (Extension)
+ (UIColor *)defaultText{
    return UIColorFromRGB(0x595959);
}
+ (UIColor *)royalBlue
{
    return UIColorFromRGB(0x2ea2cc);
}
+ (UIColor *)emeraldColor {
    return UIColorFromRGB(0x2ecc71);
}

+ (UIColor *)midNightBlueColor {
    return UIColorFromRGB(0x2c3e50);
}

+ (UIColor *)silverColor {
    return UIColorFromRGB(0xbdc3c7);
}
+ (UIColor *)belizeHoleColor {
    return UIColorFromRGB(0x2d8ac8);
}
+ (UIColor *)concreteColor {
    return UIColorFromRGB(0x95a5a6);
}
+ (UIColor *)nephritisColor {
    return UIColorFromRGB(0x27ae60);
}
+ (UIColor *)colorWithHexValue:(uint32_t)value {
    return UIColorFromRGB(value);
}
+ (UIColor *)carrotColor {
    return UIColorFromRGB(0xFD7727);
}

+ (UIColor*)primaryColor {
    return UIColorFromRGB(0x027dae);
}

+ (UIColor *)darkTransparentColor {
    return UIColorFromRGBWithAlpha(0x000000, 0.1f);
}

+ (UIColor *)luckyMoneyColor {
    return UIColorFromRGB(0xDE6254);
}

+ (UIColor *)colorWithHexValue:(uint32_t)value alpha:(float)alpha {
    return UIColorFromRGBWithAlpha(value, alpha);
}

+ (UIColor *)colorWithHexaValue:(uint)hexValue andAlpha:(float)alpha {
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:alpha];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(float)alpha {
    UIColor *col;
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@"0x"];
    uint hexValue;
    if ([[NSScanner scannerWithString:hexString] scanHexInt:&hexValue]) {
        col = [self colorWithHexaValue:hexValue andAlpha:alpha];
    } else {
        col = [self blackColor];
    }
    return col;
}
+ (UIColor*)defaultBackground{
    return UIColorFromRGB(0xe5e5e5);
}

+ (UIColor *)randomColor {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

+ (UIColor *)navigationBarColor{
    return colorFromRGB(0, 108, 171);
}
//+ (UIColor *)luckyMoneyColor {
//    return [UIColor colorWithHexValue:0xFF5722];
//}
@end
