//
//  UIColor+Extension.h
//  NewsReader
//
//  Created by Phuc  Pham Van on 3/10/14.
//  Copyright (c) 2014 NP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extension)
+ (UIColor *)defaultText;
+ (UIColor *)royalBlue;
+ (UIColor *)emeraldColor;
+ (UIColor *)concreteColor;
+ (UIColor *)nephritisColor;
+ (UIColor *)defaultBackground;
+ (UIColor *)carrotColor;

+ (UIColor*)primaryColor;
+ (UIColor *)darkTransparentColor;
+ (UIColor *)luckyMoneyColor;

+ (UIColor *)colorWithHexValue:(uint32_t)value;
+ (UIColor *)colorWithHexValue:(uint32_t)value alpha:(float)alpha;
+ (UIColor *)colorWithHexaValue:(uint)hexValue andAlpha:(float)alpha;
+ (UIColor *)colorWithHexString:(NSString *)hexString andAlpha:(float)alpha;
+ (UIColor *)randomColor;
+ (UIColor *)midNightBlueColor;
+ (UIColor *)silverColor;
+ (UIColor *)belizeHoleColor;

+ (UIColor *) navigationBarColor;

//+ (UIColor *)luckyMoneyColor;

@end
