//
//  NSData+Conversion.h
//  paymentclient
//
//  Created by tinvukhac on 12/17/15.
//  Copyright © 2015 VNG Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (NSData_Conversion)

#pragma mark - String Conversion
- (NSString *)hexadecimalString;

@end