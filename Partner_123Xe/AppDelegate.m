//
//  AppDelegate.m
//  PaymentUbusPartner
//
//  Created by tinvukhac on 2/19/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "AppDelegate.h"

#import <RestKitSANetworking@MindSea/RestKit.h>
#import "TRKJSONSerialization.h"

#import "URLConstant.h"
#import "UIColor+Extension.h"
#import "AppData.h"
#import "BaseViewController.h"
#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocompleteQuery.h>
#import "APIClient.h"

static NSString *const kHNKDemoGooglePlacesAutocompleteApiKey = @"AIzaSyDP32_ckA1f9AzxyV4xXzg7A0Os0sblGRE";


@interface AppDelegate ()
{
    UINavigationController * rootNavigationController;
}
@property (nonatomic,assign) BOOL isHaveTripCurrent;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //config api dev or live

    
    
    // Override point for customization after application launch.
    /*
     
    for (NSString *fontFamilyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:fontFamilyName]) {
            NSLog(@"Family: %@    Font: %@", fontFamilyName, fontName);
        }
    }
     */
    
    // Configure tabbar
    
    [[UITabBar appearance] setBarTintColor:[UIColor navigationBarColor]];
    //[[UITabBar appearance] setBarStyle:<#(UIBarStyle)#>];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    
    //[[UITabBar appearance] set]
    // Add this code to change StateNormal text Color,
    
    // then if StateSelected should be different, you should add this code
    [UITabBarItem.appearance setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor whiteColor]}
                                           forState:UIControlStateSelected];

    
    
    self.window.windowLevel = UIWindowLevelStatusBar + 1;
    
    // Configure RestKit
  //  [self configureRestKit];
    
    // Configure Google Places query
//    [HNKGooglePlacesAutocompleteQuery setupSharedQueryWithAPIKey: kHNKDemoGooglePlacesAutocompleteApiKey];
    
    // Register remote notification
    UIUserNotificationSettings * settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert
                                                                                         | UIUserNotificationTypeBadge
                                                                                         | UIUserNotificationTypeSound) categories:nil];
    [application registerUserNotificationSettings:settings];
    
    //    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MainFlow" bundle:nil];
    //    UIViewController * initialViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewControllerStoryboardIdentifier"];
    
  //  [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
//    if([AFNetworkReachabilityManager sharedManager].isReachable)
//    {
//        NSLog(@"Network reachable");
//    }
//    else
//    {
//        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"LoginFlow" bundle:nil];
//        initialViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewControllerStoryboardIdentifier"];
//        rootNavigationController = [[UINavigationController alloc] initWithRootViewController:initialViewController];
//        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
//        self.window.rootViewController = rootNavigationController;
//        [self.window makeKeyAndVisible];
//        return YES;
//    }
    
    
    
    //NSSetUncaughtExceptionHandler(&exceptionHandler);

   
    
    
    return YES;
}


#pragma mark- AlertView delegate


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark- Notification

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    NSLog(@"the generated device token string is : %@", deviceTokenString);
    
    [[AppData sharedInstance] setDeviceToken:deviceTokenString];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    
}



#pragma mark- Private methods

- (void)configureRestKit
{
    // Initialize AFNetworking HTTPClient
    NSURL * baseURL = [NSURL URLWithString:BASE_URL];
    SAHTTPClient * client = [[SAHTTPClient alloc] initWithBaseURL:baseURL];
    
    // Initialize RestKit
    RKObjectManager * objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    [RKMIMETypeSerialization unregisterClass:[RKNSJSONSerialization class]];
    [RKMIMETypeSerialization registerClass:[TRKJSONSerialization class] forMIMEType:RKMIMETypeJSON];
    [RKObjectManager setSharedManager:objectManager];
}

- (void)configureRestKitWithUrl:(NSString *)url
{
    // Initialize AFNetworking HTTPClient
    NSURL * baseURL = [NSURL URLWithString:url];
    SAHTTPClient * client = [[SAHTTPClient alloc] initWithBaseURL:baseURL];
    
    // Initialize RestKit
    RKObjectManager * objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    [RKObjectManager setSharedManager:objectManager];
    
    // RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    // RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
}

@end
