//
//  main.m
//  PaymentUbusPartner
//
//  Created by tinvukhac on 2/19/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
