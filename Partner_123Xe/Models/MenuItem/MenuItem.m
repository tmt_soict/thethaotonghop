//
//  MenuItem.m
//  paymentclient
//
//  Created by tinvukhac on 12/8/15.
//  Copyright © 2015 VNG Corporation. All rights reserved.
//

#import "MenuItem.h"

@implementation MenuItem

#pragma mark- Public methods

- (id)initWithTitle:(NSString *)title imageName:(NSString *)imageName
{
    self = [super init];
    if(self)
    {
        self.title = title;
        self.imageName = imageName;
    }
    
    return self;
}
- (id)initWithTitle:(NSString *)title
          imageName:(NSString *)imageName
         completion:(BlockAction)completion {
    self = [super init];
    if(self)
    {
        self.title = title;
        self.imageName = imageName;
        self.action = completion;
    }
    
    return self;
}
@end
@implementation ItemData
@end