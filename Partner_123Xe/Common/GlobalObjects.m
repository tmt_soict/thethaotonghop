//
//  GlobalObjects.m
//  PhotoEditor
//
//  Created by tinvk on 1/5/15.
//  Copyright (c) 2015 CALACULU. All rights reserved.
//

#import "GlobalObjects.h"

@implementation GlobalObjects

//CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(GlobalObjects);
static GlobalObjects *sharedObject = nil;
+ (instancetype)sharedInstance {
    if (sharedObject == nil) {
        @synchronized(self) {
            if (sharedObject == nil) {
                sharedObject = [[GlobalObjects alloc] init];
            }
        }
    }
    return sharedObject;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    
    return self;
}

- (void)updateOrCreateNewValue:(id)value forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isAlreadyExistKey:(NSString *)key
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:key])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (id)getValueForKey:(NSString *)key
{
    if([self isAlreadyExistKey:key])
    {
        return [[NSUserDefaults standardUserDefaults] valueForKey:key];
    }
    else
    {
        return nil;
    }
}

// Longitude

//Latitude




@end
