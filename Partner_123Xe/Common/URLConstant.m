//
//  URLConstant.m
//  Plus
//
//  Created by Wala on 2/16/14.
//
//

#import "URLConstant.h"

@implementation URLConstant

NSString * const BASE_URL                        =   @"http://dev2.api.123xe.vn"; //http://dev.mapi2.me.zing.vn http://api.timxekhach.com
NSString * const SESSION                         =   @"/uodapi/session";
NSString * const USER                            =   @"/uodapi/user";
NSString * const DRIVER                          =   @"/uodapi/driver";

@end
