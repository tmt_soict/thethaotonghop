//
//  AppDelegate.h
//  PaymentUbusPartner
//
//  Created by tinvukhac on 2/19/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;
- (void)configureRestKit;
- (void)configureRestKitWithUrl:(NSString *)url;
@end

