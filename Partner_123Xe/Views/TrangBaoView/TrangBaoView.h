//
//  TrangBaoView.h
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrangBaoView : UIView
@property (weak, nonatomic) IBOutlet UICollectionView *trangBaoCollectionView;

@end
