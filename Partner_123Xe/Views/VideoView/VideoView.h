//
//  VideoView.h
//  SportsNews
//
//  Created by TuTMT on 10/29/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol VideoViewDelegate<NSObject>
@end
@interface VideoView : UIView
@property (weak, nonatomic) IBOutlet UITableView *videoTableView;
@property (nonatomic,assign) id<VideoViewDelegate> delegate;

@end
