//
//  TinLienQuanView.h
//  SportsNews
//
//  Created by TuTMT on 11/1/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TinLienQuanView : UIView
@property (weak, nonatomic) IBOutlet UITableView *tinLienQuanTableView;

@end
