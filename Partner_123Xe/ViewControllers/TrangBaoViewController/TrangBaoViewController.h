//
//  TrangBaoViewController.h
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TrangBaoView.h"
#import "TrangBaoItemCollectionViewCell.h"
@interface TrangBaoViewController : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,retain) TrangBaoView* trangBaoView;

@end
