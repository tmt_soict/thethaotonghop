//
//  TrangBaoViewController.m
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "TrangBaoViewController.h"

@interface TrangBaoViewController (){
    NSArray *arrayBaoImage;
    NSArray *arrayNameBao;
    NSArray *arrayIdBao;
}

@end

@implementation TrangBaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    // Do any additional setup after loading the view.
}
-(void) initView{
    self.trangBaoView = (TrangBaoView*) self.view;
    self.trangBaoView.trangBaoCollectionView.delegate = self;
    self.trangBaoView.trangBaoCollectionView.dataSource = self;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(100, 100)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    [self.trangBaoView.trangBaoCollectionView setCollectionViewLayout:flowLayout];
    arrayNameBao = [[NSArray alloc] initWithObjects:@"Zing News",
                                                    @"Dân Trí",
                                                    @"24h", @"VOV", @"Bóng đá số", @"Bóng đá Plus", @"2Sao", @"Báo Lao động",  @"Tin tức trong ngày", @"Khám phá", @"Bóng đá 24h", @"Người lao động", @"Công lý", @"Tuổi trẻ", @"Bóng đá Infor", @"Đời sống pháp luật", @"Tin tức online", @"Việt Báo nước Nga", @"Báo Mới", @"Tiền Phong", @"Việt Nam Plus"
                    
                    , nil];
    arrayBaoImage = [[NSArray alloc] initWithObjects:@"zing", @"dantri", @"24h", @"vov", @"bongdaso", @"bongdaplus",@"2sao",@"laodong",@"infonet", @"khampha",@"bongda24h",@"nguoilaodong",@"congly",@"tuoitre",@"bongdainfo",@"doisongphapluat",@"tintuconline",@"vietbaonuocnga",@"baomoi",@"tienphong",@"vietnamplus", nil];
    [self.trangBaoView.trangBaoCollectionView registerNib:[UINib nibWithNibName:@"TrangBaoItemCollectionViewCell" bundle:nil]forCellWithReuseIdentifier:@"TrangBaoItemCollectionViewCellIdentifiers"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrayNameBao.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString* nameBao = (NSString*) arrayNameBao[indexPath.row];
    NSString* nameImage = (NSString*) arrayBaoImage[indexPath.row];
    TrangBaoItemCollectionViewCell* trangbaoItem = [collectionView dequeueReusableCellWithReuseIdentifier:@"TrangBaoItemCollectionViewCellIdentifiers" forIndexPath:indexPath];
    trangbaoItem.nameBaoLabel.text = nameBao;
    trangbaoItem.BaoImage.image = [UIImage imageNamed:nameImage];
    
    return trangbaoItem;
    
}
-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(void) selectionTrangBaoWithIndexPath: (NSInteger) indexPath{
    int lid;
    int cid;
    switch (indexPath) {
        case 0:
            lid = 1;
            cid = 22;
            break;
        case 1:
            lid = 3;
            cid = 22;
            break;
        case 2:
            lid = 4;
            cid = 23;
            break;
        case 3:
            lid = 9;
            cid = 22;
            break;
        case 4:
            lid = 16;
            cid = 999;
            break;
        case 5:
            lid = 17;
            cid = 999;
            break;
        case 6:
            lid = 19;
            cid = 22;
            break;
        case 7:
            lid = 25;
            cid = 22;
            break;
        case 8:
            lid = 35;
            cid = 22;
            break;
        case 9:
            lid = 37;
            cid = 22;
            break;
        case 10:
            lid = 45;
            cid = 999;
            break;
        case 11:
            lid = 68;
            cid = 22;
            break;
        case 12:
            lid = 85;
            cid = 22;
            break;
        case 13:
            lid = 97;
            cid = 22;
            break;
        case 14:
            lid = 101;
            cid = 999;
            break;
        case 15:
            lid = 153;
            cid = 22;
            break;
        case 16:
            lid = 160;
            cid = 22;
            break;
        case 17:
            lid = 162;
            cid = 22;
            break;
        case 18:
            lid = 163;
            cid = 22;
            break;
        case 19:
            lid = 173;
            cid = 22;
            break;
        case 20:
            lid = 174;
            cid = 22;
            break;

        default:
            break;
    }

    
}


@end
