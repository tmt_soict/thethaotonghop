//
//  HotNewsViewController.m
//  Partner_123Xe
//
//  Created by TuTMT on 10/21/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "HotNewsViewController.h"
#import "RootDetailArticleViewController.h"
@interface HotNewsViewController (){
    NSNumber *lastLid;
}
@end

@implementation HotNewsViewController{
    NSMutableArray *arrayNews;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayNews = [[NSMutableArray alloc] init];
    [self initHotNewsView];
    // Do any additional setup after loading the view.
}

-(void)initHotNewsView{
    self.hotNewsView = (HotNewsView*) self.view;
    self.hotNewsView.delegate = self;
    self.hotNewsView.hotNewTableView.delegate = self;
    self.hotNewsView.hotNewTableView.dataSource = self;
    [self.hotNewsView.hotNewTableView setDragDelegate:self refreshDatePermanentKey:@"HotNewsList"];
    self.hotNewsView.hotNewTableView.showRefreshView = YES;
    [self.hotNewsView.hotNewTableView registerNib:[UINib nibWithNibName:@"HotNewItemType2TableViewCell" bundle:nil] forCellReuseIdentifier:@"HotNewItemType2TableViewCell"];
     [self.hotNewsView.hotNewTableView registerNib:[UINib nibWithNibName:@"HotNewsItemType1TableViewCell" bundle:nil] forCellReuseIdentifier:@"HotNewsItemType1TableViewCellIdentifiers"];
    lastLid = [NSNumber numberWithInt:0];
}
-(void) setUpNotData{
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,  self.hotNewsView.hotNewTableView.bounds.size.width, self.hotNewsView.hotNewTableView.bounds.size.height)];
    noDataLabel.text             = @"Không có chuyến sắp chạy nào";
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    //yourTableView.backgroundView = noDataLabel;
    //yourTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.hotNewsView.hotNewTableView.backgroundView = noDataLabel;
     self.hotNewsView.hotNewTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadDataWithIsLoad:NO];

}
-(void)loadDataWithIsLoad:(BOOL) isload{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [self.apiClient getArticleWithSid:@"999" cid:@"22" count:[NSNumber numberWithInt:30] meta:YES lastLid:lastLid completionHandler:^(Data *output, NSError *error, double errorCode) {
        [MBProgressHUD hideHUDForView:self.view animated:true];

        if (error == nil) {
            
            if ((int)errorCode == SUCCSESS) {
                if (isload) {
                    arrayNews = [[NSMutableArray alloc] init];
                }
                for (int i =0 ; i < output.linfos.count; i++) {
                    [arrayNews addObject:output.linfos[i]];
                }
                lastLid = output.lids[output.lids.count-1];
                if (arrayNews.count == 0) {
                    [self setUpNotData];
                }
                else{
                    [self.hotNewsView.hotNewTableView reloadData];
                }

            }
            
        }
        else{
            
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark UITableView Delegatetable view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayNews.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row ==0 || indexPath.row % 5 == 0)  {
        return 150;
    }
    return 100;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Linfos* linfos = (Linfos*) arrayNews[indexPath.row];
    if(indexPath.row ==0 || indexPath.row % 5 == 0){
        
        HotNewItemType2TableViewCell *hotNewItemType2TableViewCell = [tableView
                                                                      dequeueReusableCellWithIdentifier:@"HotNewItemType2TableViewCell"
                                                                      forIndexPath:indexPath];
        
        [hotNewItemType2TableViewCell setUpHotNewsItemType1TableViewCellWithLinfos:linfos];
        return hotNewItemType2TableViewCell;
    }
    else{
        
        HotNewsItemType1TableViewCell *hotNewType1 =  [tableView
                                                       dequeueReusableCellWithIdentifier:@"HotNewsItemType1TableViewCellIdentifiers"
                                                       forIndexPath:indexPath];
        [hotNewType1 setUpHotNewsItemType1TableViewCellWithLinfos:linfos];
        return hotNewType1;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Linfos* linfos = (Linfos*) arrayNews[indexPath.row];
    [self.appData setUrl:linfos.url];
    [self.appData setTitle:linfos.title];
    [self.appData setDesc:linfos.desc];
    [self.appData setPosttime:[NSNumber numberWithDouble:linfos.posttime]];
    [self.appData setContent:linfos.content];
    NSLog(@"content: %@",linfos.content);
    [self performSegueWithIdentifier:@"HotNewsToDetailArticleIdentifiersSegue" sender:nil];
    
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"HotNewsToDetailArticleIdentifiersSegue"]) {
        RootDetailArticleViewController*  rootDetailArticleViewController = (RootDetailArticleViewController*) segue.destinationViewController;
        rootDetailArticleViewController.hidesBottomBarWhenPushed = YES;
        [rootDetailArticleViewController setTitle:@"Tin mới nhất"];
    }
}
#pragma -mark DragLoad delegate 
- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}
-(void)finishLoadMore{
    [self loadDataWithIsLoad:NO];
    [self.hotNewsView.hotNewTableView finishLoadMore];
}
-(void) finishRefresh{
    lastLid = [NSNumber numberWithInt:0];
    [self loadDataWithIsLoad:YES];
    [self.hotNewsView.hotNewTableView finishRefresh];
}
@end
