//
//  HotNewsViewController.h
//  Partner_123Xe
//
//  Created by TuTMT on 10/21/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "HotNewsView.h"
#import "HotNewItemType2TableViewCell.h"
#import "HotNewsItemType1TableViewCell.h"
@interface HotNewsViewController : BaseViewController<HotNewsViewDelegate,UITableViewDelegate, UITableViewDataSource,UITableViewDragLoadDelegate>
@property (nonatomic,retain) HotNewsView* hotNewsView;

@end
