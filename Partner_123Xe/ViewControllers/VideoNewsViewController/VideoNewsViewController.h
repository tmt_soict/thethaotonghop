//
//  VideoNewsViewController.h
//  SportsNews
//
//  Created by TuTMT on 10/29/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "VideoView.h"
#import "VideoItemType2TableViewCell.h"
#import "VideoItemViewType1TableViewCell.h"
#import "RootDetailArticleViewController.h"

@interface VideoNewsViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UITableViewDragLoadDelegate>
@property (nonatomic,assign) VideoView* videoView;

@end
