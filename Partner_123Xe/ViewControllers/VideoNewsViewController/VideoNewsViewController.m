//
//  VideoNewsViewController.m
//  SportsNews
//
//  Created by TuTMT on 10/29/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "VideoNewsViewController.h"

@interface VideoNewsViewController (){
    NSNumber *lastLid;
    NSMutableArray *arrayVideo;
}

@end

@implementation VideoNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayVideo = [[NSMutableArray alloc] init];
    
    [self initViewVideoView];
    // Do any additional setup after loading the view.
}

-(void) initViewVideoView{
    self.videoView = (VideoView*) self.view;
    //self.videoView.delegate = self;
    self.videoView.videoTableView.delegate = self;
    self.videoView.videoTableView.dataSource = self;
    [self.videoView.videoTableView setDragDelegate:self refreshDatePermanentKey:@"VideoNewsList"];
    self.videoView.videoTableView.showRefreshView = YES;
    ;
    [self.videoView.videoTableView registerNib:[UINib nibWithNibName:@"VideoItemType2TableViewCell" bundle:nil] forCellReuseIdentifier:@"VideoItemViewType2TableViewCellIdentifiers"];
    [self.videoView.videoTableView registerNib:[UINib nibWithNibName:@"VideoItemViewType1TableViewCell" bundle:nil] forCellReuseIdentifier:@"VideoItemViewType1TableViewCellIdentifiers"];
    lastLid = [NSNumber numberWithInt:0];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) setUpNotData{
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,  self.videoView.videoTableView.bounds.size.width, self.videoView.videoTableView.bounds.size.height)];
    noDataLabel.text             = @"Không có chuyến sắp chạy nào";
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    //yourTableView.backgroundView = noDataLabel;
    //yourTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.videoView.videoTableView.backgroundView = noDataLabel;
    self.videoView.videoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadDataWithIsLoad:NO];
}
-(void)loadDataWithIsLoad:(BOOL) isload{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [self.apiClient getArticleWithSid:@"45" cid:@"1806" count:[NSNumber numberWithInt:30] meta:YES lastLid:lastLid completionHandler:^(Data *output, NSError *error, double errorCode) {
        [MBProgressHUD hideHUDForView:self.view animated:true];
        
        if (error == nil) {
            
            if ((int)errorCode == SUCCSESS) {
                if (isload) {
                    arrayVideo = [[NSMutableArray alloc] init];
                }
                for (int i =0 ; i < output.linfos.count; i++) {
                    [arrayVideo addObject:output.linfos[i]];
                }
                lastLid = output.lids[output.lids.count-1];
                if (arrayVideo.count == 0) {
                    [self setUpNotData];
                }
                else{
                    [self.videoView.videoTableView reloadData];
                }
                
            }
            
        }
        else{
            
        }
    }];
}


#pragma -mark UITable View Delegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  arrayVideo.count;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row ==0)  {
        return 150;
    }
    return 230;
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Linfos* linfos = (Linfos*) arrayVideo[indexPath.row];
    if(indexPath.row ==0){
        
        VideoItemType2TableViewCell *videoItemType2TableViewCell = [tableView
                                                                      dequeueReusableCellWithIdentifier:@"VideoItemViewType2TableViewCellIdentifiers"
                                                                      forIndexPath:indexPath];
        
        [videoItemType2TableViewCell setUpHotNewsItemType1TableViewCellWithLinfos:linfos];
        return videoItemType2TableViewCell;
    }
    else{
        
        VideoItemViewType1TableViewCell *videoItemViewType1TableViewCell =  [tableView
                                                       dequeueReusableCellWithIdentifier:@"VideoItemViewType1TableViewCellIdentifiers"
                                                       forIndexPath:indexPath];
        [videoItemViewType1TableViewCell setUpHotNewsItemType1TableViewCellWithLinfos:linfos];
        return videoItemViewType1TableViewCell;
    }

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"VideoNewsToDetailArticleIdentifiers" sender:nil];
    Linfos* linfos = (Linfos*) arrayVideo[indexPath.row];
    [self.appData setUrl:linfos.url];
    [self.appData setTitle:linfos.title];
    [self.appData setDesc:linfos.desc];
    [self.appData setPosttime:[NSNumber numberWithDouble:linfos.posttime]];
    [self.appData setContent:linfos.content];
    NSLog(@"content: %@",linfos.content);

}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"VideoNewsToDetailArticleIdentifiers"]) {
        RootDetailArticleViewController*  rootDetailArticleViewController = (RootDetailArticleViewController*) segue.destinationViewController;
        rootDetailArticleViewController.hidesBottomBarWhenPushed = YES;
        [rootDetailArticleViewController setTitle:@"Video tổng hợp"];
    }
}
#pragma -mark Dragload Delegate 
- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}
-(void)finishLoadMore{
    [self loadDataWithIsLoad:NO];
    [self.videoView.videoTableView finishLoadMore];
}
-(void) finishRefresh{
    lastLid = [NSNumber numberWithInt:0];
    [self loadDataWithIsLoad:YES];
    [self.videoView.videoTableView finishRefresh];
}

@end
