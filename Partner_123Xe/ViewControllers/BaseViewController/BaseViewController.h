//
//  BaseViewController.h
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//
//

#import <UIKit/UIKit.h>
#import "APIClient.h"
#import "Util.h"
#import "Logging.h"
#import "AppData.h"
#import "Macro.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#import "HTTPStatusCodes.h"
#import <SDWebImage/SDImageCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <REFrostedViewController/REFrostedViewController.h>
#import <MessageUI/MessageUI.h> 

typedef enum {
    kSearchTypeViewFull,
    kSearchTypeViewByName,
    kSearchTypeViewByCategory,
    kSearchTypeViewByPrice
    
} kSearchType;

@interface BaseViewController : UIViewController<UIAlertViewDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, weak) APIClient *apiClient;
@property (nonatomic, weak) AppDelegate *appDelegate;
@property (nonatomic, weak) AppData *appData;
@property (strong, nonatomic) MBProgressHUD     *progress;

-(BOOL) findErrorWithCode:(NSNumber *)code LocalizedDescription :(NSString *) localizedDescription;
-(BOOL) isErrorWithCode: (NSNumber *) code Error:(NSError *) error;

- (void)setupUiBarForSlideMenuWithImageName:(NSString *)imageNamed;
-(void) showMenu;
-(void) pushToLoginViewControler;

@end
