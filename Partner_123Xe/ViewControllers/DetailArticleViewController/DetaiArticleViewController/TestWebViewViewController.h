//
//  TestWebViewViewController.h
//  SportsNews
//
//  Created by TuTMT on 11/3/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestWebViewViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
