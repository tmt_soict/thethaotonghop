//
//  DetailArticleViewController.h
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <REFrostedViewController/REFrostedViewController.h>
#import "BaseViewController.h"
#import <WebKit/WebKit.h>
@protocol DetailArticleViewControllerDelegate<NSObject>
-(void)renewsData;
@end
@interface DetailArticleViewController : BaseViewController<UIWebViewDelegate>
@property (nonatomic, assign) id<DetailArticleViewControllerDelegate> delegate;
@property (nonatomic,retain) Linfos* linfos;
- (IBAction)panGestureRecognize:(id)sender;

@end
