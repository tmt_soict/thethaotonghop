//
//  TinLienQuanViewController.m
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "TinLienQuanViewController.h"

@interface TinLienQuanViewController (){
    NSNumber *lastLid;
    NSMutableArray *arrayNews;
}

@end

@implementation TinLienQuanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self loadDataWithIsLoad:YES];

    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}
-(void)initView{
    self.tinLienQuanView = (TinLienQuanView*) self.view;
    self.tinLienQuanView.tinLienQuanTableView.delegate = self;
    self.tinLienQuanView.tinLienQuanTableView.dataSource = self;
    [self.tinLienQuanView.tinLienQuanTableView setDragDelegate:self refreshDatePermanentKey:@"TinLienQuanList"];
    self.tinLienQuanView.tinLienQuanTableView.showRefreshView = YES;
    [self.tinLienQuanView.tinLienQuanTableView registerNib:[UINib nibWithNibName:@"HotNewItemType2TableViewCell" bundle:nil]  forCellReuseIdentifier:@"HotNewItemType2TableViewCell"];
    [self.tinLienQuanView.tinLienQuanTableView registerNib:[UINib nibWithNibName:@"HotNewsItemType1TableViewCell"bundle:nil] forCellReuseIdentifier:@"HotNewsItemType1TableViewCellIdentifiers"];
    lastLid = [NSNumber numberWithInteger:0];
}

-(void) setUpNotData{
    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,  self.tinLienQuanView.tinLienQuanTableView.bounds.size.width, self.tinLienQuanView.tinLienQuanTableView.bounds.size.height)];
    noDataLabel.text             = @"Không có chuyến sắp chạy nào";
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    //yourTableView.backgroundView = noDataLabel;
    //yourTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tinLienQuanView.tinLienQuanTableView.backgroundView = noDataLabel;
    self.tinLienQuanView.tinLienQuanTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadDataWithIsLoad:(BOOL) isload{
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    [self.apiClient getArticleWithSid:@"999" cid:@"22" count:[NSNumber numberWithInt:30] meta:NO lastLid:lastLid completionHandler:^(Data *output, NSError *error, double errorCode) {
        [MBProgressHUD hideHUDForView:self.view animated:true];
        
        if (error == nil) {
            
            if ((int)errorCode == SUCCSESS) {
                if (isload) {
                    arrayNews = [[NSMutableArray alloc] init];
                }
                for (int i =0 ; i < output.linfos.count; i++) {
                    [arrayNews addObject:output.linfos[i]];
                }
                lastLid = output.lids[output.lids.count-1];
                if (arrayNews.count == 0) {
                    [self setUpNotData];
                }
                else{
                    [self.tinLienQuanView.tinLienQuanTableView reloadData];
                }
                
            }
            
        }
        else{
            
        }
    }];
}

#pragma -UITableViewDelegate 
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return arrayNews.count;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}
-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Linfos* linfos = (Linfos*) arrayNews[indexPath.row];
    HotNewsItemType1TableViewCell *hotNewType1 =  [tableView
                                                   dequeueReusableCellWithIdentifier:@"HotNewsItemType1TableViewCellIdentifiers"
                                                   forIndexPath:indexPath];
    [hotNewType1 setUpHotNewsItemType1TableViewCellWithLinfos:linfos];
    return hotNewType1;
}
-(UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* headerView = [[UIView alloc] init];
    headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 60);
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel* lable = [[UILabel alloc] init];
    lable.text = @"Tin liên quan";
    [lable setTextColor:[UIColor blackColor]];
    lable.frame = CGRectMake(self.view.frame.size.width/2 - 30,20, 100, 20);
    [headerView addSubview:lable];
    
    UIView* lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor lightGrayColor];
    lineView.frame = CGRectMake(0, 50, self.view.frame.size.width, 1);
    [headerView addSubview:lineView];
    return headerView;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.frostedViewController hideMenuViewController];
    Linfos* linfos = (Linfos*) arrayNews[indexPath.row];
    [self.appData setUrl:linfos.url];
    [self.appData setTitle:linfos.title];
    [self.appData setDesc:linfos.desc];
    [self.appData setPosttime:[NSNumber numberWithDouble:linfos.posttime]];
    [self.appData setContent:linfos.content];
    [self.delegates didSelectArticleRelation];
    //    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailArticleViewControllerStoryboardIdentifiers"];
}
#pragma -mark Dragload Delegate
- (void)dragTableDidTriggerRefresh:(UITableView *)tableView
{
    //send refresh request(generally network request) here
    
    [self performSelector:@selector(finishRefresh) withObject:nil afterDelay:2];
}

- (void)dragTableRefreshCanceled:(UITableView *)tableView
{
    //cancel refresh request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishRefresh) object:nil];
}

- (void)dragTableDidTriggerLoadMore:(UITableView *)tableView
{
    //send load more request(generally network request) here
    
    [self performSelector:@selector(finishLoadMore) withObject:nil afterDelay:2];
}

- (void)dragTableLoadMoreCanceled:(UITableView *)tableView
{
    //cancel load more request(generally network request) here
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishLoadMore) object:nil];
}
-(void)finishLoadMore{
    [self loadDataWithIsLoad:NO];
    [self.tinLienQuanView.tinLienQuanTableView finishLoadMore];
}
-(void) finishRefresh{
    lastLid = [NSNumber numberWithInt:0];
    [self loadDataWithIsLoad:YES];
    [self.tinLienQuanView.tinLienQuanTableView finishRefresh];
}

@end
