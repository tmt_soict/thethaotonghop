//
//  TinLienQuanViewController.h
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TinLienQuanView.h"
#import "HotNewsItemType1TableViewCell.h"
@protocol TinLienQuanViewControllerDelegate<NSObject>
-(void)didSelectArticleRelation;
@end
@interface TinLienQuanViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UITableViewDragLoadDelegate>
@property (nonatomic,assign) id<TinLienQuanViewControllerDelegate> delegates;
@property (nonatomic,retain) TinLienQuanView* tinLienQuanView;
@end
