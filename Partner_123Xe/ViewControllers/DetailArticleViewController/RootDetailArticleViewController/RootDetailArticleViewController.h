//
//  RootDetailArticleViewController.h
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DetailArticleViewController.h"
#import "TinLienQuanViewController.h"
@interface RootDetailArticleViewController :REFrostedViewController<TinLienQuanViewControllerDelegate,DetailArticleViewControllerDelegate>
@property (nonatomic, retain) NSString* title;
- (IBAction)backButtonTouchUpInside:(id)sender;

@end
