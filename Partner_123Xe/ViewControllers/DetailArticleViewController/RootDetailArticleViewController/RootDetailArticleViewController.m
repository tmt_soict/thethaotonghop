//
//  RootDetailArticleViewController.m
//  SportsNews
//
//  Created by TuTMT on 10/31/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "RootDetailArticleViewController.h"

@interface RootDetailArticleViewController ()

@end

@implementation RootDetailArticleViewController

-(void)awakeFromNib{
    DetailArticleViewController*  detailArticleViewController = (DetailArticleViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"DetailArticleViewControllerStoryboardIdentifiers"];

    self.contentViewController = detailArticleViewController;
    detailArticleViewController.delegate = self;
    TinLienQuanViewController* tinLienQuanViewController = (TinLienQuanViewController*) [self.storyboard instantiateViewControllerWithIdentifier:@"TinLienQuanViewControllerStoryboardIdentifiers"];
    tinLienQuanViewController.delegates = self;
    self.menuViewController = tinLienQuanViewController;
    [self setDirection:REFrostedViewControllerDirectionRight];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setTitle:self.title];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark TinLienQuan Delegate 
-(void) didSelectArticleRelation{
    
}
-(void) renewsData{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonTouchUpInside:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
@end
