//
//  MainTabBarViewController.h
//  Partner_123Xe
//
//  Created by TuTMT on 10/21/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MainTabBarViewControllerDelegate <NSObject>
-(void) pushToAnotherViewControllerWithIdentifiers:(NSString*) indentifiers;
@end
@interface MainTabBarViewController : UITabBarController<UITabBarControllerDelegate>

@end
