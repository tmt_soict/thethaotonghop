//
//  LocationService.h
//  iCompanion
//
//  Created by tinvukhac on 4/13/13.
//  Copyright (c) 2013 SIG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationService : NSObject<CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) BOOL isUpdatingLocation;
@property (nonatomic, retain) CLLocation *currentLocation;

- (void)updateLocation;
- (void)updateLocationWithTimeOut:(NSTimeInterval)timeOut;
- (void)updateLocationWithTimeOut:(NSTimeInterval)timeOut
               andDesiredAccuracy:(CLLocationAccuracy)accuracy;

+ (LocationService*)Shared;

@end
