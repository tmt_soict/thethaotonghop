//
//  LocationService.m
//  iCompanion
//
//  Created by tinvukhac on 4/13/13.
//  Copyright (c) 2013 SIG. All rights reserved.
//

#import "Global.h"
#import "LocationService.h"

@interface LocationService()

@property (strong, nonatomic) CLLocation *bestEffortAtLocation;

- (void)stopUpdatingLocation:(NSString *)state;
- (void)getLocationSuccess:(CLLocation *)location;
- (void)getLocationError:(CLLocation *)location;

@end

@implementation LocationService

static LocationService* gInstance = nil;

+ (LocationService*)Shared {
	if( !gInstance )
	{
		gInstance = [[LocationService alloc] init];
	}
	return gInstance;
}

- (void)updateLocation
{
    // update location with default time out of 10 seconds
    [self updateLocationWithTimeOut:10];
}

- (void)updateLocationWithTimeOut:(NSTimeInterval)timeOut
{
    // update location with default accuracy of Kilometer
    [self updateLocationWithTimeOut:timeOut
                 andDesiredAccuracy:kCLLocationAccuracyKilometer];
}

- (void)updateLocationWithTimeOut:(NSTimeInterval)timeOut
               andDesiredAccuracy:(CLLocationAccuracy)accuracy
{
    if (!self.isUpdatingLocation)
    {
        self.isUpdatingLocation = YES;
        self.bestEffortAtLocation = nil;    // Reset the best location
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.desiredAccuracy = accuracy;
        self.locationManager.delegate = self;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            //[self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
        
        [self performSelector:@selector(stopUpdatingLocation:)
                   withObject:@"Timed Out"
                   afterDelay:timeOut];    // timeout
        
        // Notify observer that the manager will start updating location
        //[[NSNotificationCenter defaultCenter] postNotificationName:kBeginUpdatingLocationNotification
        //                                                    object:self];
        
        gInstance = self;
    }
}

- (void)stopUpdatingLocation:(NSString *)state
{
    // cancel our previous performSelector:withObject:afterDelay: - it's no longer necessary
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(stopUpdatingLocation:)
                                               object:@"Timed Out"];
    
    if ([state isEqualToString:@"Timed Out"] && !self.bestEffortAtLocation)
    {   // if at time out there's still no good location then
        // we decrease the desired accuracy
        self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
        return;
    }
    
    self.isUpdatingLocation = NO;
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    
    self.currentLocation = [self coordinateCheckOfLocation:self.bestEffortAtLocation];
    
    if ([state isEqualToString:@"Timed Out"] ||
        [state isEqualToString:@"Success"])
    {
        [self getLocationSuccess:self.currentLocation];
    }
    else
    {
        [self getLocationError:self.currentLocation];
    }
}

- (void)getLocationSuccess:(CLLocation *)location
{
    // Notifies observer that the manager got the final location
//    [[NSNotificationCenter defaultCenter] postNotificationName:kGetLocationSuccessNotification
//                                                        object:location];
}

- (void)getLocationError:(CLLocation *)location
{
    // Failed
//    [[NSNotificationCenter defaultCenter] postNotificationName:kGetLocationErrorNotification
//                                                        object:location];
}

/**
 Check if this coordinate is in Vietnam or not
 @return    Coordinate of location parameter if it is in Vietnam, coordinate of Hanoi if not
 
 */
- (CLLocation *)coordinateCheckOfLocation:(CLLocation *)location
{
    if (location.coordinate.latitude > 8 &&
        location.coordinate.latitude < 24 &&
        location.coordinate.longitude > 102 &&
        location.coordinate.longitude < 110)
    {
        return location;
    }
    else
    {
        return [[CLLocation alloc] initWithLatitude:21.028253
                                          longitude:105.852624];
    }
}

#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = manager.location;
    
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 5.0) return;
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) return;
    // test the measurement to see if it is more accurate than the previous measurement
    if (self.bestEffortAtLocation == nil ||
        self.bestEffortAtLocation.horizontalAccuracy > newLocation.horizontalAccuracy) {
        // store the location as the "best effort"
        self.bestEffortAtLocation = newLocation;
        // test the measurement to see if it meets the desired accuracy
        //
        // IMPORTANT!!! kCLLocationAccuracyBest should not be used for comparison with location coordinate or altitidue
        // accuracy because it is a negative value. Instead, compare against some predetermined "real" measure of
        // acceptable accuracy, or depend on the timeout to stop updating. This sample depends on the timeout.
        //
        //TODO: uncomment below to get desiredAccuracy
        if (newLocation.horizontalAccuracy <= manager.desiredAccuracy) {
            // we have a measurement that meets our requirements, so we can stop updating the location
            //
            // IMPORTANT!!! Minimize power usage by stopping the location manager as soon as possible.
            //
            [self stopUpdatingLocation:@"Success"];
        }
    }
}

-(void)locationManager:(CLLocationManager *)manager
      didFailWithError:(NSError *)error{
    NSLog(@"LocationService: Could not get user location: %@",error);
    [self stopUpdatingLocation:@"Failed"];
}


@end
