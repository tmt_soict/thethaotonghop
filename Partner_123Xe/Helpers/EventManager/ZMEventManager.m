//
//  ZMEventManager.m
//  ZingMeNetwork
//
//  Created by Bon on 10/2/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import "ZMEventManager.h"

#import <RACSubscriber.h>
#import <RACDisposable.h>
#import <RACEXTScope.h>
#import <RACSignal+Operations.h>
#import <RACSubject.h>
#import <RACScheduler.h>

#import <NSObject+RACDeallocating.h>


@interface ZMEventManager ()
@property (nonatomic, strong) RACSubject *internalSignal;
@end

@implementation ZMEventManager

+ (ZMEventManager *) sharedInstance {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

- (id)init {
    self = [super init];
    if (self) {
        self.internalSignal = [RACSubject subject];
    }
    return self;
}

+ (void)registerHandler:(id)handler eventType:(int)eventType withBlock:(ZMEventHandler)blockHandle {
    [[[[ZMEventManager sharedInstance].internalSignal takeUntil:[handler rac_willDeallocSignal]]
      filter:^BOOL(ZMEvent *event) {
        return event.eventType == eventType;
    }] subscribeNext:^(ZMEvent *event) {
        blockHandle(event);
    }];
}


+ (void)bindEvent:(int)eventype
             data:(id)data {
    [[RACScheduler scheduler] schedule:^{
        ZMEvent *event = [ZMEvent eventWithType:eventype data:data];
        [[ZMEventManager sharedInstance].internalSignal sendNext:event];
    }];

}


@end
@implementation ZMEvent

+ (instancetype)eventWithType:(int)eventType data:(id)eventData {
    return [[ZMEvent alloc ] initWithType:eventType data:eventData];
}

- (id)initWithType:(int)type data:(id)data {
    self = [super init];
    if (self) {
        _eventType = type;
        _eventData  = data;
    }
    return self;
}
@end