//
//  ZMEventManager.h
//  ZingMeNetwork
//
//  Created by Bon on 10/2/15.
//  Copyright (c) 2015 Bon. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    EventTypeUpdateBalance,
    EventTransferSuccess,
    EventTransferFail,
    EventWalletBeginUpdate,
    EventWalletFinishUpdate
} EventType;

@class ZMEvent;
typedef  void (^ZMEventHandler)(ZMEvent *event);
@interface ZMEvent : NSObject
@property (nonatomic, readonly) int eventType;
@property (nonatomic, readonly) id eventData;
+ (instancetype)eventWithType:(int)eventType data:(id)eventData;
@end

@interface ZMEventManager : NSObject

+ (void)registerHandler:(id)handler
              eventType:(int)eventType
              withBlock:(ZMEventHandler)blockHandle;

+ (void)bindEvent:(int)eventype
             data:(id)data;
@end
