//
//  Util.m
//  Shoppie
//
//  Created by Le Quang Vinh on 11/13/13.
//  
//

#import "Util.h"
#import "Macro.h"
#import "Global.h"

#define kCalendarType NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit | NSWeekOfMonthCalendarUnit | NSWeekOfYearCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSTimeZoneCalendarUnit

@implementation Util

+ (Util *)sharedUtil {
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[self alloc] init];
    });
}

- (void)dealloc {
    self.progressView = nil;
    [super dealloc];
}

+ (AppDelegate *)appDelegate {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate;
}

#pragma mark set coder radius 
+ (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

#pragma mark Alert functions
+ (void)showMessage:(NSString *)message withTitle:(NSString *)title {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title andDelegate:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alert show];
    [alert release];
}

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title delegate:(id)delegate andTag:(NSInteger)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = tag;
    [alert show];
    [alert release];
}

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title delegate:(id)delegate titleButton:(NSString *) titleButton andTag:(NSInteger)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:titleButton otherButtonTitles:nil, nil];
    alert.tag = tag;
    [alert show];
    [alert release];
}

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle otherButtonTitles:(NSString *)otherTitle delegate:(id)delegate andTag:(NSInteger)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelTitle otherButtonTitles:otherTitle, nil];
    alert.tag = tag;
    alert.delegate = delegate;
    [alert show];
    [alert release];
}

+ (void)showMessage:(NSString *)message withTitle:(NSString *)title style:(UIAlertViewStyle)style cancelButtonTitle:(NSString *)cancelTitle otherButtonTitles:(NSString *)otherTitle delegate:(id)delegate andTag:(NSInteger)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:cancelTitle otherButtonTitles:otherTitle, nil];
    alert.alertViewStyle = style;
    alert.tag = tag;
    alert.delegate = delegate;
    [alert show];
    [alert release];
}

#pragma mark Date functions
+ (NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [formatter setDateFormat:dateFormat];
    NSDate *ret = [formatter dateFromString:dateString];
    [formatter release];
    return ret;
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    NSString *ret = [formatter stringFromDate:date];
    [formatter release];
    return ret;
}

+ (NSString *)stringFromDateString:(NSString *)dateString
{
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *utcDate = [formatter dateFromString:dateString];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    [formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"] autorelease]];
    [formatter setDateFormat:@"MM/dd/yyyy h:mm a"];
    return [formatter stringFromDate:utcDate];
}

+ (NSDate*)convertTwitterDateToNSDate:(NSString*)created_at
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"EEE LLL d HH:mm:ss Z yyyy"];
	[dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"US"]];
	
	NSDate *convertedDate = [dateFormatter dateFromString:created_at];
	[dateFormatter release];
	
    return convertedDate;
}

+ (NSString *)stringFromDateRelative:(NSDate*)date {
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	[dateFormatter setDateStyle: NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
	[dateFormatter setDoesRelativeDateFormatting:YES];
	
	NSString *result = [dateFormatter stringFromDate:date];
	
	return result;
}

#pragma mark Loading View
- (MBProgressHUD *)progressView
{
    if (!_progressView) {
        _progressView = [[MBProgressHUD alloc] initWithView:[Util appDelegate].window];
        _progressView.animationType = MBProgressHUDAnimationFade;
        _progressView.dimBackground = NO;
		[[Util appDelegate].window addSubview:_progressView];
    }
    return _progressView;
}

- (void)showLoadingView {
    [self hideLoadingView];
    [self showLoadingViewWithTitle:@""];
}

- (void)showLoadingViewWithTitle:(NSString *)title
{
    [self hideLoadingView];
    self.progressView.labelText = title;
    [self.progressView show:NO];
}

- (void)hideLoadingView {
    [self.progressView hide:NO];
}

#pragma mark NSUserDefaults functions
+ (void)setValue:(id)value forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
    [[NSUserDefaults standardUserDefaults] setValue:value forKeyPath:keyPath];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setObject:(id)obj forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:obj forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (id)valueForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

+ (id)valueForKeyPath:(NSString *)keyPath
{
    return [[NSUserDefaults standardUserDefaults] valueForKeyPath:keyPath];
}

+ (id)objectForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (void)removeObjectForKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark JSON functions
+ (id)convertJSONToObject:(NSString*)str {
	NSError *error = nil;
	NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *responseDict;
	
	if (data) {
		responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	} else {
		responseDict = nil;
	}
	
	return responseDict;
}

+ (NSString *)convertObjectToJSON:(id)obj {
	NSError *error = nil;
	NSData *data = [NSJSONSerialization dataWithJSONObject:obj options:NSJSONWritingPrettyPrinted error:&error];
	
	if (error) {
		return @"";
	}
	return [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
}

+ (id)getJSONObjectFromFile:(NSString *)file {
	NSString *textPAth = [[NSBundle mainBundle] pathForResource:file ofType:@"json"];
	
    NSError *error;
    NSString *content = [NSString stringWithContentsOfFile:textPAth encoding:NSUTF8StringEncoding error:&error];  //error checking omitted
	
	id returnData = [Util convertJSONToObject:content];
	return returnData;
}

#pragma mark Other stuff
+ (NSString *)getXIB:(Class)fromClass
{
	NSString *className = NSStringFromClass(fromClass);
	
	if (IS_IPAD()) {
		className = [className stringByAppendingString:IPAD_XIB_POSTFIX];
	} else {
		
	}
	return className;
}

#pragma mark- My helper (tinvk)
+ (int)getIntValueWithDictionary:(NSDictionary*)dict forKey:(NSString*)key
{
    if([dict valueForKey:key] == [NSNull null])
    {
        DLog(@"get int: Null value, key = %@", key);
        return 0;
    }
    else
    {
        return [[dict valueForKey:key] intValue];
    }
}

+ (float)getFloatValueWithDictionary:(NSDictionary*)dict forKey:(NSString*)key
{
    if([dict valueForKey:key] == [NSNull null])
    {
        DLog(@"get float: Null value, key = %@", key);
        return 0;
    }
    else
    {
        return [[dict valueForKey:key] floatValue];
    }
}

+ (double)computeDistanceWithLat1:(double)lat1 long1:(double)long1 lat2:(double)lat2 long2:(double)long2
{
    double dlong = ((long2 - long1) * d2r);
    double dlat = (lat2 - lat1) * d2r;
    double a = pow(sin(dlat/2.0), 2) + cos(lat1*d2r) * cos(lat2*d2r) * pow(sin(dlong/2.0), 2);
    double c = 2 * atan2(sqrt(a), sqrt(1-a));
    double d = 6367 * c;
    
    return d;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (NSString*)convertToCurrencyWithNumber:(int)intNumber
{
    NSString *stringNumber= [NSString stringWithFormat:@"%d", intNumber];
    NSMutableArray *invertCharArray = [[NSMutableArray alloc] init];
    for(NSInteger i = stringNumber.length - 1; i >= 0; i--)
    {
        [invertCharArray addObject:[NSString stringWithFormat:@"%C", [stringNumber characterAtIndex:i]]];
    }
    NSMutableArray *invertResultCharArray = [[NSMutableArray alloc] init];
    for(int i = 0; i < invertCharArray.count; i++)
    {
        [invertResultCharArray addObject:[invertCharArray objectAtIndex:i]];
        if(((i + 1) % 3 == 0) && ((i + 1) < invertCharArray.count))
        {
            [invertResultCharArray addObject:@","];
        }
    }
    
    NSString *result = @"";
    for(NSInteger i = invertResultCharArray.count - 1; i >= 0; i--)
    {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@", [invertResultCharArray objectAtIndex:i]]];
    }
    
    return result;
}

+ (NSString*)convertToCurrencyWithFloat:(float)floatNumber
{
    NSString *stringNumber= [NSString stringWithFormat:@"%.0f", floatNumber];
    NSMutableArray *invertCharArray = [[NSMutableArray alloc] init];
    for(NSInteger i = stringNumber.length - 1; i >= 0; i--)
    {
        [invertCharArray addObject:[NSString stringWithFormat:@"%C", [stringNumber characterAtIndex:i]]];
    }
    NSMutableArray *invertResultCharArray = [[NSMutableArray alloc] init];
    for(int i = 0; i < invertCharArray.count; i++)
    {
        [invertResultCharArray addObject:[invertCharArray objectAtIndex:i]];
        if(((i + 1) % 3 == 0) && ((i + 1) < invertCharArray.count))
        {
            [invertResultCharArray addObject:@","];
        }
    }
    
    NSString *result = @"";
    for(NSInteger i = invertResultCharArray.count - 1; i >= 0; i--)
    {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@", [invertResultCharArray objectAtIndex:i]]];
    }
    
    return result;
}

+ (NSString*)convertToCurrencyWithString:(NSString *)stringNumber
{
    NSMutableArray *invertCharArray = [[NSMutableArray alloc] init];
    for(NSInteger i = stringNumber.length - 1; i >= 0; i--)
    {
        [invertCharArray addObject:[NSString stringWithFormat:@"%C", [stringNumber characterAtIndex:i]]];
    }
    NSMutableArray *invertResultCharArray = [[NSMutableArray alloc] init];
    for(NSInteger i = 0; i < invertCharArray.count; i++)
    {
        [invertResultCharArray addObject:[invertCharArray objectAtIndex:i]];
        if(((i + 1) % 3 == 0) && ((i + 1) < invertCharArray.count))
        {
            [invertResultCharArray addObject:@","];
        }
    }
    
    NSString *result = @"";
    for(NSInteger i = invertResultCharArray.count - 1; i >= 0; i--)
    {
        result = [result stringByAppendingString:[NSString stringWithFormat:@"%@", [invertResultCharArray objectAtIndex:i]]];
    }
    
    return result;
}

+ (NSString*)formatToSeparatedStyleWithString:(NSString *)str
{
    if(str.length > 0)
    {
        NSMutableArray *strCharArray = [[NSMutableArray alloc] init];
        for(NSInteger i = 0; i < str.length; i++)
        {
            [strCharArray addObject:[NSString stringWithFormat:@"%C", [str characterAtIndex:i]]];
        }
        
        NSMutableArray *resultCharArray = [[NSMutableArray alloc] init];
        
        for(NSInteger i = 0; i < strCharArray.count; i++)
        {
            [resultCharArray addObject:[strCharArray objectAtIndex:i]];
            if(((i + 1) % 4 == 0) && ((i + 1) < strCharArray.count))
            {
                [resultCharArray addObject:@"-"];
            }
        }
        
        NSString *result = @"";
        for(NSInteger i = 0; i < resultCharArray.count; i++)
        {
            result = [result stringByAppendingString:[NSString stringWithFormat:@"%@", [resultCharArray objectAtIndex:i]]];
        }
        
        return result;
    }
    else
    {
        return str;
    }
}

+ (NSString*)stringWithDashSeparated:(NSString*)str {
    NSMutableString *string = [NSMutableString stringWithString:str];
    NSUInteger index = 4;
    while (index < [string length]) {
        [string insertString:@"-" atIndex:index];
        index += 5;
    }
    return string;
}

+ (BOOL)isValidNickname:(NSString*)input
{
    NSString *nicknameRegex = @"[a-zA-Z0-9_]{4,16}";
    NSPredicate *nicknameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nicknameRegex];
    return [nicknameTest evaluateWithObject:input];
}

+ (CGSize) computeSizeWithString:(NSString *)input font:(UIFont *)font
{
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(296,9999);
    
    CGSize expectedLabelSize = [input sizeWithFont:font
                                      constrainedToSize:maximumLabelSize
                                          lineBreakMode:NSLineBreakByWordWrapping];
    
    return expectedLabelSize;
}

+ (UIImage *) imageForAsset:(ALAsset*) aAsset{
    ALAssetRepresentation *rep;
    rep = [aAsset defaultRepresentation];
    return [UIImage imageWithCGImage:[rep fullResolutionImage]];
}

+ (UIImage *) fullScreenImageForAsset:(ALAsset*) aAsset{
    ALAssetRepresentation *rep;
    rep = [aAsset defaultRepresentation];
    return [UIImage imageWithCGImage:[rep fullScreenImage]];
}

+ (CGImageRef)resizeCGImage:(CGImageRef)image toWidth:(unsigned long)width andHeight:(unsigned long)height {
    // create context, keeping original image properties
    CGColorSpaceRef colorspace = CGImageGetColorSpace(image);
    CGContextRef context = CGBitmapContextCreate(NULL, width, height,
                                                 CGImageGetBitsPerComponent(image),
                                                 CGImageGetBytesPerRow(image),
                                                 colorspace,
                                                 CGImageGetAlphaInfo(image));
    CGColorSpaceRelease(colorspace);
    
    
    if(context == NULL)
        return nil;
    
    
    // draw image to context (resizing it)
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image);
    // extract resulting image from context
    CGImageRef imgRef = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    
    return imgRef;
}

+ (NSString *)getStoryBoardName
{
    NSString * storyBoardName = @"Main_iPhone5";
//    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
//    switch ((int)iOSDeviceScreenSize.height) {
//        case 480:
//            storyBoardName = @"Main_iPhone35";
//            break;
//            
//        case 568:
//            storyBoardName = @"Main_iPhone5";
//            break;
//            
//        case 667:
//            storyBoardName = @"Main_iPhone6";
//            break;
//            
//        case 1104:
//            storyBoardName = @"Main_iPhone6p";
//            break;
//            
//        default:
//            storyBoardName = @"Main_iPhone5";
//            break;
//    }
    
    return storyBoardName;
}

// Get navigation controller with id
+ (UINavigationController *)getNavigationControllerWithIdentifier:(NSString *)identifier storyboardName:(NSString *)storyboardName
{
    if((storyboardName == nil) || (storyboardName.length == 0))
    {
        storyboardName = @"Main";
    }
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    UINavigationController * navigationController = (UINavigationController*)[storyboard instantiateViewControllerWithIdentifier:identifier];
    return navigationController;
}

// App version
+ (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (NSString *) build
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}

+ (NSString *) versionBuild
{
    NSString * version = [Util appVersion];
    NSString * build = [Util build];
    
    NSString * versionBuild = [NSString stringWithFormat: @"v%@", version];
    
    if (![version isEqualToString: build]) {
        versionBuild = [NSString stringWithFormat: @"%@(%@)", versionBuild, build];
    }
    
    return versionBuild;
}

// UIImage
/*Drawimage in image*/
+(UIImage*) drawImage:(UIImage*)fgImage inImage:(UIImage*)bgImage atPoint:(CGPoint)point
{
    UIGraphicsBeginImageContextWithOptions(bgImage.size, FALSE, 0.0);
    [bgImage drawInRect:CGRectMake( 0, 0, bgImage.size.width, bgImage.size.height)];
    [fgImage drawInRect:CGRectMake( point.x, point.y, fgImage.size.width, fgImage.size.height)];
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

/*Create image width color*/
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/*Create image width color and size*/
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size {
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/*Create QR image from a string*/
+ (CIImage *)createQRForString:(NSString *)qrString
{
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"L" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    return qrFilter.outputImage;
}


@end
