//
//  MenuPopupViewController.m
//  Passenger_123Xe
//
//  Created by TuTMT on 6/29/16.
//  Copyright © 2016 VNG Corporation. All rights reserved.
//

#import "MenuPopupViewController.h"

@interface MenuPopupViewController (){
    int numberCheck;
}
- (void)updateCell:(MenuPopupTableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath;

@end

@implementation MenuPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       [self.tableView registerNib:[UINib nibWithNibName:@"MenuPopupTableViewCell" bundle:nil] forCellReuseIdentifier:@"MenuPopupTableViewCell"];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void) setDataWithUIView: (UIView *) yourUIView
            urlCheckImage: (NSString *) urlCheckImage
           listContenMenu: (NSArray *) menuArray{
    self.menuArray = menuArray;
    self.urlCheckImage = urlCheckImage;
    self.yourUIView = yourUIView;
    //[self.tableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _menuArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *JourneyListTableViewCellIdentifier = @"MenuPopupTableViewCell";
    MenuPopupTableViewCell * cell = [tableView
                                     dequeueReusableCellWithIdentifier:JourneyListTableViewCellIdentifier
                                     forIndexPath:indexPath];
    
    cell.titleLable.text = _menuArray[indexPath.row];

    if (indexPath.row == 0) {
            cell.checkImageView.image =  [UIImage imageNamed:_urlCheckImage];
        
    }
    else{
        cell.checkImageView.image = nil;
        
    }
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    
    MenuPopupTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    numberCheck = indexPath.row;
    if([self.delegate respondsToSelector:@selector(SendActionWithIndex:)]) {
        
        [self.delegate SendActionWithIndex:indexPath.row];
    }
    for (int i = 0 ; i < _menuArray.count; i++) {
        indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [self updateCell:cell atIndexPath:indexPath];
    }
    


}

#pragma mark private

- (void)updateCell:(MenuPopupTableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.section == 0) {
        if (numberCheck == indexPath.row) {
            cell.checkImageView.image =  [UIImage imageNamed:_urlCheckImage];
        }
        else
        {
            cell.checkImageView.image = nil;
            
        }
    }
    
}

@end
